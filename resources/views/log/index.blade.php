@extends('template')

@section('content')
    <div class="page-content">
        <!-- BEGIN BREADCRUMBS -->
        <div class="breadcrumbs">
            <h1>Logs</h1>
            <ol class="breadcrumb">
                <li>
                    <a href="{{ route('home') }}">Home</a>
                </li>
                <li>
                    <a href="">Management</a>
                </li>
                <li class="active">
                    Logs
                </li>
            </ol>
        </div>
        <!-- END BREADCRUMBS -->
        <!-- BEGIN CONTENT -->
        <div class="row">
            <form action="{{ url()->current() }}">
                <div class="col-md-4">
                    @if($keyword != null)
                        <input type="text" class="form-control" name="search" placeholder="Cari..." value="{{ $keyword }}">
                    @else
                        <input type="text" class="form-control" name="search" placeholder="Cari...">
                    @endif
                </div>
                <div class="col-md-2">
                    <button type="submit" class="btn blue">Cari</button>
                    @php $route_name = \Request::route()->getName(); @endphp
                    <a href="{{ route($route_name) }}" class="btn default">Reset</a>
                </div>
            </form>
        </div><br>
        @if ($keyword != NULL)
            <div class="row">
                <div class="col-md-12">
                    <div class="alert alert-info">
                        <p>Menampilkan hasil pencarian untuk <b>"{{ $keyword }}"</b></p>
                    </div>
                </div>
            </div>
        @endif
        <div class="row">
            <div class="col-md-12">
                <div class="table-scrollable">
                    <table class="table table-striped table-bordered table-hover">
                        <tr>
                            <th width="5%"><center>No</center></th>
                            <th width="80%"><center>Log</center></th>
                            <th width="15%"><center>Waktu</center></th>
                        </tr>
                        @foreach($logs as $l)
                            <tr>
                                <td align="center">{{ $loop->iteration }}</td>
                                <td>{{ $l->log }}</td>
                                <td>{{ $l->updated_at }}</td>
                            </tr>
                        @endforeach
                    </table>
                </div>
            </div>
        </div>
        <!-- END CONTENT -->
    </div>
@endsection