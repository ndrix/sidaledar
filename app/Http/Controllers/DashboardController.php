<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Barang;
use App\Sektor;
use App\Log;
use Carbon\Carbon;

class DashboardController extends Controller
{
    public function index()
    {
        $total_modul_sandi                      = Barang::whereHas('kategori', function($query) { $query->where('jenis_id', 1); })->count();
        $total_perangkat_tik                    = Barang::whereHas('kategori', function($query) { $query->where('jenis_id', 2); })->count();
        $modul_sandi_tersertifikasi             = Barang::whereHas('kategori', function($query) { $query->where('jenis_id', 1); })->where('keterangan', 'sudah')->count();
        $perangkat_tik_tersertifikasi           = Barang::whereHas('kategori', function($query) { $query->where('jenis_id', 2); })->where('keterangan', 'sudah')->count();
        $sektor                                 = Sektor::all();
        $logs                                   = Log::whereMonth('created_at', Carbon::now()->month)->get();

        if($total_modul_sandi == 0){
            $persentase_sertifikasi_modul_sandi = 0;
        } else {
            $persentase_sertifikasi_modul_sandi     = $modul_sandi_tersertifikasi / $total_modul_sandi * 100;
        }
        if($total_perangkat_tik == 0){
            $persentase_sertifikasi_perangkat_tik = 0;
        } else{
            $persentase_sertifikasi_perangkat_tik   = $perangkat_tik_tersertifikasi / $total_perangkat_tik * 100;
        }

        return view('analisis', ['total_modul_sandi' => $total_modul_sandi, 'total_perangkat_tik' => $total_perangkat_tik, 'modul_sandi_tersertifikasi' => $modul_sandi_tersertifikasi, 
        'perangkat_tik_tersertifikasi' => $perangkat_tik_tersertifikasi, 'persentase_sertifikasi_modul_sandi' => $persentase_sertifikasi_modul_sandi, 
        'persentase_sertifikasi_perangkat_tik' => $persentase_sertifikasi_perangkat_tik, 'sektor' => $sektor, 'logs' => $logs]);
    }

    public function home()
    {
        return view('dashboard');
    }
}
