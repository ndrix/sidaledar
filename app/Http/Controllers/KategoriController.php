<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Kategori;
use App\JenisBarang;
use App\Http\Requests\KategoriRequest;
use Auth;

class KategoriController extends Controller
{
    public function index(Request $request)
    {
        $keyword                    = $request->get('search');

        //Fungsi search di halaman index
        if($keyword != null){
            $kategori               = Kategori::where('kategori', 'like', '%'.$keyword.'%')->get();
        } else {
            $kategori               = Kategori::all();
        }

        return view('kategori.index', ['kategori' => $kategori, 'keyword' => $keyword]);
    }

    public function create()
    {
        $jenis                      = JenisBarang::all();
        return view('kategori.create', ['jenis' => $jenis]);
    }

    public function store(KategoriRequest $request)
    {
        $kategori['kategori']       = $request->get('kategori');
        $kategori['jenis_id']       = $request->get('jenis_id');
        Kategori::create($kategori);

        $log['jenis'] = "add";
        $log['log']   = Auth::user()->name." melakukan penambahan kategori.";
        \App\Log::create($log);

        return redirect()->back()->with('success', 'Berhasil menambah kategori baru.');
    }

    public function edit(Kategori $kategori)
    {
        $jenis                      = JenisBarang::all();
        return view('kategori.edit', ['kategori' => $kategori, 'jenis' => $jenis]);
    }

    public function update(Kategori $kategori, KategoriRequest $request)
    {
        $kategori->kategori         = $request->get('kategori');
        $kategori->jenis_id         = $request->get('jenis_id');
        $kategori->save();

        $log['jenis'] = "update";
        $log['log']   = Auth::user()->name." melakukan pengubahan data kategori.";
        \App\Log::create($log);

        return redirect()->back()->with('success', 'Berhasil mengubah data kategori.');
    }

    public function destroy(Kategori $kategori)
    {
        $kategori->delete();

        $log['jenis'] = "delete";
        $log['log']   = Auth::user()->name." melakukan penghapusan data kategori.";
        \App\Log::create($log);

        return redirect()->back()->with('success', 'Berhasil menghapus data kategori.');
    }
}
