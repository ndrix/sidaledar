<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kategori extends Model
{
    protected $table = 'kategoris';

    protected $fillable = [
        'jenis_id', 'kategori'
    ];

    public function jenis()
    {
        return $this->belongsTo('App\JenisBarang', 'jenis_id');
    }

    public function barang()
    {
        return $this->hasMany('App\Barang', 'kategori_id');
    }
}
