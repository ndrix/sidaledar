<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JenisBarang extends Model
{
    protected $table = 'jenis_barangs';

    protected $fillable = [
        'jenis'
    ];

    public function kategori()
    {
        return $this->hasMany('App\Kategori', 'jenis_id');
    }
}
