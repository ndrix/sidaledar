<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class LokasiSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('lokasis')->insert(array(
            array(
                'sektor_id'     => 1,
                'lokasi'        => 'Kementerian Luar Negeri',
                'created_at'    => Carbon::now(),
                'updated_at'    => Carbon::now(),
            ),
            array(
                'sektor_id'     => 1,
                'lokasi'        => 'Kementerian Kelautan dan Perikanan',
                'created_at'    => Carbon::now(),
                'updated_at'    => Carbon::now(),
            ),
            array(
                'sektor_id'     => 2,
                'lokasi'        => 'Pemerintah Kabupaten Gorontalo',
                'created_at'    => Carbon::now(),
                'updated_at'    => Carbon::now(),
            ),
            array(
                'sektor_id'     => 2,
                'lokasi'        => 'Pemerintah Kabupaten Mojokerto',
                'created_at'    => Carbon::now(),
                'updated_at'    => Carbon::now(),
            ),
            array(
                'sektor_id'     => 3,
                'lokasi'        => 'PT. Pertamina (Pusat)',
                'created_at'    => Carbon::now(),
                'updated_at'    => Carbon::now(),
            ),
            array(
                'sektor_id'     => 3,
                'lokasi'        => 'PT. Adhi Karya',
                'created_at'    => Carbon::now(),
                'updated_at'    => Carbon::now(),
            ),
            array(
                'sektor_id'     => 4,
                'lokasi'        => 'Bank Mandiri KCP Jakarta Ragunan',
                'created_at'    => Carbon::now(),
                'updated_at'    => Carbon::now(),
            ),
            array(
                'sektor_id'     => 4,
                'lokasi'        => 'Bank BCA KCP Jakarta Cilandak',
                'created_at'    => Carbon::now(),
                'updated_at'    => Carbon::now(),
            ),
            array(
                'sektor_id'     => 5,
                'lokasi'        => 'Laboratorium Universitas Indonesia',
                'created_at'    => Carbon::now(),
                'updated_at'    => Carbon::now(),
            ),
            array(
                'sektor_id'     => 5,
                'lokasi'        => 'Laboratorium Universitas Negeri Jakarta',
                'created_at'    => Carbon::now(),
                'updated_at'    => Carbon::now(),
            ),
            array(
                'sektor_id'     => 6,
                'lokasi'        => 'PT. Semen Holcim',
                'created_at'    => Carbon::now(),
                'updated_at'    => Carbon::now(),
            ),
            array(
                'sektor_id'     => 6,
                'lokasi'        => 'PT. Kalbe Farma',
                'created_at'    => Carbon::now(),
                'updated_at'    => Carbon::now(),
            ),
        ));
    }
}
