@extends('template')

@section('content')
    <div class="page-content">
        <!-- BEGIN BREADCRUMBS -->
        <div class="breadcrumbs">
            <h1>Dashboard</h1>
            <ol class="breadcrumb">
                <li>
                    <a href="{{ route('home') }}">Home</a>
                </li>
                <li class="active">
                    Dashboard
                </li>
            </ol>
        </div>
        <!-- END BREADCRUMBS -->
        <!-- BEGIN PAGE BASE CONTENT -->
        <div class="row">
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="dashboard-stat2 bordered">
                    <div class="display">
                        <div class="number">
                            <h3 class="font-green-sharp">
                                <span data-counter="counterup" data-value="{{ $total_modul_sandi }}">0</span>
                                <small class="font-green-sharp">Unit</small>
                            </h3>
                            <small>Total Modul Sandi</small>
                        </div>
                        <div class="icon">
                            <i class="icon-shield"></i>
                        </div>
                    </div>
                    <div class="progress-info">
                        <div class="progress">
                            <!-- mal nanti span style widthnya menggunakan perhitungan persentase ya, ganti yg width 76% yg dibawah -->
                            <span style="width: 75%;" class="progress-bar progress-bar-success green-sharp">
                                <span class="sr-only">75% progress</span>
                            </span>
                        </div>
                        <div class="status">
                            <div class="status-title"> Progress Pendataan </div>
                            <div class="status-number"> 75% </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="dashboard-stat2 bordered">
                    <div class="display">
                        <div class="number">
                            <h3 class="font-red-haze">
                                <span data-counter="counterup" data-value="{{ $total_perangkat_tik }}">0</span>
                            </h3>
                            <small>Total Perangkat TI</small>
                        </div>
                        <div class="icon">
                            <i class="icon-screen-desktop"></i>
                        </div>
                    </div>
                    <div class="progress-info">
                        <div class="progress">
                            <span style="width: 85%;" class="progress-bar progress-bar-success red-haze">
                                <span class="sr-only">85% change</span>
                            </span>
                        </div>
                        <div class="status">
                            <div class="status-title"> Progress Pendataan </div>
                            <div class="status-number"> 85% </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="dashboard-stat2 bordered">
                    <div class="display">
                        <div class="number">
                            <h3 class="font-blue-sharp">
                                <span data-counter="counterup" data-value="{{ $modul_sandi_tersertifikasi }}"></span>
                            </h3>
                            <small>Modul Sandi Tersertifikasi</small>
                        </div>
                        <div class="icon">
                            <i class="fa fa-line-chart"></i>
                        </div>
                    </div>
                    <div class="progress-info">
                        <div class="progress">
                            <span style="width: {{ $persentase_sertifikasi_modul_sandi }}%;" class="progress-bar progress-bar-success blue-sharp">
                                <span class="sr-only">{{ $persentase_sertifikasi_modul_sandi."%" }} grow</span>
                            </span>
                        </div>
                        <div class="status">
                            <div class="status-title"> Persentase </div>
                            <div class="status-number"> {{ $persentase_sertifikasi_modul_sandi."%" }} </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="dashboard-stat2 bordered">
                    <div class="display">
                        <div class="number">
                            <h3 class="font-purple-soft">
                                <span data-counter="counterup" data-value="{{ $perangkat_tik_tersertifikasi }}"></span>
                            </h3>
                            <small>Perangkat TI Tersertifikasi</small>
                        </div>
                        <div class="icon">
                            <i class="fa fa-bar-chart"></i>
                        </div>
                    </div>
                    <div class="progress-info">
                        <div class="progress">
                            <span style="width: {{ $persentase_sertifikasi_perangkat_tik }}%;" class="progress-bar progress-bar-success purple-soft">
                                <span class="sr-only">{{ $persentase_sertifikasi_perangkat_tik."%" }} change</span>
                            </span>
                        </div>
                        <div class="status">
                            <div class="status-title"> Persentase </div>
                            <div class="status-number"> {{ $persentase_sertifikasi_perangkat_tik."%" }} </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6 col-xs-12 col-sm-12">
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption ">
                            <span class="caption-subject font-dark bold uppercase">Jumlah Perangkat Tersertifikasi Tiap Bulan</span>
                            <span class="caption-helper"></span>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <canvas id="ChartOne" width="400" height="400"></canvas>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-xs-12 col-sm-12">
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption ">
                            <span class="caption-subject font-dark bold uppercase">Jumlah Perangkat Tersertifikasi di Tiap Sektor</span>
                            <span class="caption-helper"></span>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <canvas id="ChartTwo" width="400" height="400"></canvas>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 col-xs-12 col-sm-12">
                <!-- BEGIN PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title tabbable-line">
                        <div class="caption">
                            <i class="icon-globe font-dark hide"></i>
                            <span class="caption-subject font-dark bold uppercase">Log</span>
                        </div>
                        <ul class="nav nav-tabs">
                            <li class="active">
                                <a href="#tab_1_1" class="active" data-toggle="tab"> Activities </a>
                            </li>
                        </ul>
                    </div>
                    <div class="portlet-body">
                        <!--BEGIN TABS-->
                        <div class="tab-content">
                            <div class="tab-pane active" id="tab_1_1">
                                <div class="scroller" style="height: 339px;" data-always-visible="1" data-rail-visible="0">
                                    <ul class="feeds">
                                        @foreach($logs as $l)  
                                            <li>
                                                <div class="col1">
                                                    <div class="cont">
                                                        <div class="cont-col1">
                                                            @switch($l->jenis)
                                                                @case($l->jenis == "login")
                                                                    <div class="label label-sm label-success">
                                                                        <i class="fa fa-sign-in"></i>
                                                                    </div>
                                                                    @break
                                                                @case($l->jenis == "logout")
                                                                    <div class="label label-sm label-danger">
                                                                        <i class="fa fa-sign-out"></i>
                                                                    </div>
                                                                    @break
                                                                @case($l->jenis == "add")
                                                                    <div class="label label-sm label-primary">
                                                                        <i class="fa fa-plus"></i>
                                                                    </div>
                                                                    @break
                                                                @case($l->jenis == "update")
                                                                    <div class="label label-sm label-default">
                                                                        <i class="fa fa-pencil"></i>
                                                                    </div>
                                                                    @break
                                                                @case($l->jenis == "delete")
                                                                    <div class="label label-sm label-warning">
                                                                        <i class="fa fa-trash"></i>
                                                                    </div>
                                                                    @break
                                                            @endswitch
                                                        </div>
                                                        <div class="cont-col2">
                                                            <div class="desc"> {{ $l->log }}
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col2">
                                                    @php
                                                        $created_at = $l->created_at->diffForHumans();
                                                        $created_at = str_replace([' seconds', ' second'], ' sec', $created_at);
                                                        $created_at = str_replace([' minutes', ' minute'], ' mins', $created_at);
                                                        $created_at = str_replace([' ago'], '', $created_at);
                                                    @endphp
                                                    <div class="date"> {{ $created_at }} </div>
                                                </div>
                                            </li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <!--END TABS-->
                    </div>
                </div>
                <!-- END PORTLET-->
            </div>
        </div>
        <!-- END PAGE BASE CONTENT -->
    </div>
@endsection
@section('scripts')
<script src="{{ asset('pages/scripts/charts-chartjsbundle.min.js') }}"></script>
<script>
    var ctx = document.getElementById("ChartTwo");
    var chartTwo = new Chart(ctx, {
        type: 'pie',
        data: {
            labels: [
                <?php
                    foreach($sektor as $s){
                        echo '"'.$s->sektor.'"'.","; 
                    }
                ?>
            ],
            datasets: [{
                    data: [
                        <?php
                            foreach($sektor as $s){
                                $lokasi                         = \App\Lokasi::where('sektor_id', $s->id)->get();
                                foreach($lokasi as $l){
                                    $jml_per_lokasi[]           = \App\Transaksi::where('lokasi_id', $l->id)->whereHas('barang', function($query){ $query->where('keterangan', 'sudah'); })->count();
                                }
                                $jml_per_sektor                 = array_sum($jml_per_lokasi);
                                $jml_per_lokasi                 = [];
                                echo "'".$jml_per_sektor."',";
                            }
                        ?>
                    ],
                    backgroundColor: [ 
                        <?php
                            foreach($sektor as $s){
                                $color  = "'"."rgba(".rand(0,255).", ".rand(0,255).", ".rand(0,255).", 0.2)',";
                                echo $color;
                            }
                        ?>
                    ],
                    borderColor: [
                        <?php
                            foreach($sektor as $s){
                                $color  = "'"."rgba(".rand(0,255).", ".rand(0,255).", ".rand(0,255).", 0.2)',";
                                echo $color;
                            }
                        ?>
                    ],
                    borderWidth: 1
                }]
        },
        options: {
            responsive: true,
            maintainAspectRatio: false,
            scales: {
                yAxes: [{
                        ticks: {
                            beginAtZero: true
                        },
                        gridLines: {
                            display: false
                        },
                        display: false,
                    }]
            }
        }
    });
</script>
<script>
    var ctx = document.getElementById("ChartOne");
    var chartOne = new Chart(ctx, {
        type: 'bar',
        data: {
            labels: [
                <?php
                    $bulan = ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'];
                    foreach($bulan as $b){
                        echo '"'.$b.'"'.","; 
                    }
                ?>
            ],
            datasets: [{
                    label: 'Jumlah',
                    data: [
                        <?php
                            for($i=1; $i<=12; $i++){
                                $jml_barang = \App\Barang::whereMonth('updated_at', $i)->count();
                                echo "'".$jml_barang."',";
                            }
                        ?>
                    ],
                    backgroundColor: [ 
                        <?php
                            foreach($sektor as $s){
                                $color  = "'"."rgba(".rand(0,255).", ".rand(0,255).", ".rand(0,255).", 0.2)',";
                                echo $color;
                            }
                        ?>
                    ],
                    borderColor: [
                        <?php
                            foreach($sektor as $s){
                                $color  = "'"."rgba(".rand(0,255).", ".rand(0,255).", ".rand(0,255).", 0.2)',";
                                echo $color;
                            }
                        ?>
                    ],
                    borderWidth: 1
                }]
        },
        options: {
            responsive: true,
            maintainAspectRatio: false,
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true,
                    },
                    gridLines: {
                        display: true
                    },
                    display: true,
                }],
                xAxes: [{
                    ticks: {
                        autoSkip: false
                    },
                }]
            }
        }
    });
</script>
@endsection