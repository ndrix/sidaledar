@extends('template')

@section('content')
    <link href="{{ asset('pages/css/blog.min.css') }}" rel="stylesheet" type="text/css" />
    <div class="page-content">
        <!-- BEGIN BREADCRUMBS -->
        <div class="breadcrumbs">
            {{-- <h1>Dashboard</h1> --}}
            <ol class="breadcrumb">
                <li>
                    <a href="{{ route('home') }}">Home</a>
                </li>
                <li class="active">
                    Dashboard
                </li>
            </ol>
        </div>
        <!-- END BREADCRUMBS -->
        <!-- BEGIN PAGE BASE CONTENT -->
        <div class="blog-page blog-content-2">
            <div class="row">
                <div class="col-lg-12">
                    <div class="blog-single-content bordered blog-container">
                        <div class="blog-single-head">
                            <h1 class="blog-single-head-title">Sistem Informasi Pengendalian Peredaran Produk Modul Sandi dan Keamanan Perangkat Teknologi Informasi (SIPeDATI)</h1>
                        </div>
                        <div class="blog-single-desc">
                            <p> Sistem Informasi Pengendalian Peredaran Produk Modul Sandi dan Keamanan Perangkat Teknologi Informasi (SIPeDATI) adalah paket aplikasi yang berisi pusat informasi dan data nasional tentang sebaran produk modul sandi dan keamanan perangkat teknologi informasi (TI) yang masuk atau akan beredar di dalam wilayah NKRI.   
                                Penggunaan Sistem informasi ini dimaksudkan tidak hanya untuk memetakan sebaran produk modul sandi dan keamanan perangkat TI, tetapi juga dapat memenuhi tersedianya sebuah database yang mampu menginformasikan jenis, spesifikasi, pengguna, status sertifikasi produk dan produsen/penyedia produk. Aplikasi ini secara administrasi dapat mempercepat dan mempermudah proses pencarian dan menekan sekecil mungkin human error maupun duplikasi data.
                                SIPeDATI berisikan informasi yang bersifat terbatas (classified). Informasi yang diperoleh dari SIPeDATI dapat dimanfaatkan bagi internal Badan Siber dan Sandi Negara (BSSN) sebagai bahan penyusunan kebijakan dan analisis larangan dan pembatasan, identifikasi aset serta analisis resiko keamanan informasi guna menjamin keamanan informasi di Indonesia.
                            </p>                                
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END PAGE BASE CONTENT -->
    </div>
@endsection