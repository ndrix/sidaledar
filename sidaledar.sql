-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Dec 16, 2018 at 07:04 PM
-- Server version: 10.1.31-MariaDB
-- PHP Version: 7.2.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sidaledar`
--

-- --------------------------------------------------------

--
-- Table structure for table `barangs`
--

CREATE TABLE `barangs` (
  `id` int(10) UNSIGNED NOT NULL,
  `kategori_id` int(10) UNSIGNED NOT NULL,
  `produsen_id` int(10) UNSIGNED NOT NULL,
  `merk` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tipe` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `keterangan` enum('sudah','belum') COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `barangs`
--

INSERT INTO `barangs` (`id`, `kategori_id`, `produsen_id`, `merk`, `tipe`, `keterangan`, `created_at`, `updated_at`) VALUES
(3, 2, 1, 'Kriptosoft', 'Pro', 'belum', '2018-08-23 06:22:31', '2018-08-23 06:22:31'),
(4, 2, 2, 'Omnisoft', 'Pro', 'belum', '2018-08-23 06:22:54', '2018-08-23 06:26:33'),
(5, 2, 1, 'Lazada', 'Pro', 'belum', '2018-08-23 06:38:20', '2018-08-23 06:38:20');

-- --------------------------------------------------------

--
-- Table structure for table `jenis_barangs`
--

CREATE TABLE `jenis_barangs` (
  `id` int(10) UNSIGNED NOT NULL,
  `jenis` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `jenis_barangs`
--

INSERT INTO `jenis_barangs` (`id`, `jenis`, `created_at`, `updated_at`) VALUES
(1, 'Modul Sandi', '2018-07-31 21:33:50', '2018-07-31 21:33:50'),
(2, 'Perangkat TIK', '2018-07-31 21:33:50', '2018-07-31 21:33:50');

-- --------------------------------------------------------

--
-- Table structure for table `kategoris`
--

CREATE TABLE `kategoris` (
  `id` int(10) UNSIGNED NOT NULL,
  `jenis_id` int(10) UNSIGNED NOT NULL,
  `kategori` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `kategoris`
--

INSERT INTO `kategoris` (`id`, `jenis_id`, `kategori`, `created_at`, `updated_at`) VALUES
(1, 1, 'Modul Kriptografi Hardware', '2018-07-31 21:33:50', '2018-07-31 21:33:50'),
(2, 1, 'Modul Kriptografi Software', '2018-07-31 21:33:50', '2018-07-31 21:33:50'),
(3, 1, 'Modul Kriptografi Firmware', '2018-07-31 21:33:50', '2018-07-31 21:33:50'),
(4, 1, 'Modul Kriptografi Software Hybrid', '2018-07-31 21:33:50', '2018-07-31 21:33:50'),
(5, 1, 'Modul Kriptografi Firmware Hybrid', '2018-07-31 21:33:50', '2018-07-31 21:33:50'),
(6, 2, 'Access Control and System - Identify Management', '2018-07-31 21:33:50', '2018-07-31 21:33:50'),
(7, 2, 'Access Control and System - Authentication System', '2018-07-31 21:33:50', '2018-07-31 21:33:50'),
(8, 2, 'Key Management System - Active Directory', '2018-07-31 21:33:50', '2018-07-31 21:33:50'),
(9, 2, 'Key Management System - Configuration Management System', '2018-07-31 21:33:50', '2018-07-31 21:33:50'),
(10, 2, 'Key Management System - Public Key Infrastructure', '2018-07-31 21:33:50', '2018-07-31 21:33:50'),
(11, 2, 'Key Management System - Digital Certificate', '2018-07-31 21:33:50', '2018-07-31 21:33:50'),
(12, 2, 'Key Management System - Passport System', '2018-07-31 21:33:50', '2018-07-31 21:33:50'),
(13, 2, 'Network System and Device - VPN, Proxy, Firewall, Router', '2018-07-31 21:33:50', '2018-07-31 21:33:50'),
(14, 2, 'Network System and Device - Unified Threat Management', '2018-07-31 21:33:50', '2018-07-31 21:33:50'),
(15, 2, 'Network System and Device - Network Performance System', '2018-07-31 21:33:50', '2018-07-31 21:33:50'),
(16, 2, 'Network System and Device - Switch', '2018-07-31 21:33:50', '2018-07-31 21:33:50'),
(17, 2, 'Network System and Device - Intrusion Detection System', '2018-07-31 21:33:50', '2018-07-31 21:33:50'),
(18, 2, 'Network System and Device - Intrusion Prevention System', '2018-07-31 21:33:50', '2018-07-31 21:33:50'),
(19, 2, 'Data Protection - Data and File Encryption', '2018-07-31 21:33:50', '2018-07-31 21:33:50'),
(20, 2, 'Data Protection - Securing Data Detection', '2018-07-31 21:33:50', '2018-07-31 21:33:50'),
(21, 2, 'Wireless and Mobile Devices System - Access Point Management System', '2018-07-31 21:33:50', '2018-07-31 21:33:50'),
(22, 2, 'Wireless and Mobile Device System - Access id/key encryption (wired equvalent privacy(WEP)/wi-fi protection access(WPA))', '2018-07-31 21:33:50', '2018-07-31 21:33:50'),
(23, 2, 'Smart Card and Related Device - Smart Card Chip', '2018-07-31 21:33:50', '2018-07-31 21:33:50'),
(24, 2, 'Smart Card and Related Device - Smart Card Applet', '2018-07-31 21:33:50', '2018-07-31 21:33:50'),
(25, 2, 'Smart Card and Related Device - Smart Card Reader', '2018-07-31 21:33:50', '2018-07-31 21:33:50'),
(26, 2, 'Smart Card and Related Device - Smart Card Operating System', '2018-07-31 21:33:50', '2018-07-31 21:33:50');

-- --------------------------------------------------------

--
-- Table structure for table `logs`
--

CREATE TABLE `logs` (
  `id` int(10) UNSIGNED NOT NULL,
  `jenis` enum('add','update','delete','login','logout') COLLATE utf8mb4_unicode_ci NOT NULL,
  `log` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `logs`
--

INSERT INTO `logs` (`id`, `jenis`, `log`, `created_at`, `updated_at`) VALUES
(1, 'login', 'Administrator melakukan login.', '2018-08-10 08:42:54', '2018-08-10 08:42:54'),
(2, 'logout', 'Administrator melakukan logout.', '2018-08-10 08:46:28', '2018-08-10 08:46:28'),
(3, 'login', 'Administrator melakukan login.', '2018-08-10 08:47:14', '2018-08-10 08:47:14'),
(4, 'logout', 'Administrator melakukan logout.', '2018-08-10 09:44:49', '2018-08-10 09:44:49'),
(5, 'login', 'Administrator melakukan login.', '2018-08-10 09:54:47', '2018-08-10 09:54:47'),
(6, 'login', 'Administrator melakukan login.', '2018-08-10 09:54:55', '2018-08-10 09:54:55'),
(7, 'logout', 'Administrator melakukan logout.', '2018-08-10 09:55:01', '2018-08-10 09:55:01'),
(8, 'login', 'Administrator melakukan login.', '2018-08-10 09:55:28', '2018-08-10 09:55:28'),
(9, 'logout', 'Administrator melakukan logout.', '2018-08-10 09:55:36', '2018-08-10 09:55:36'),
(10, 'login', 'Administrator melakukan login.', '2018-08-10 09:57:05', '2018-08-10 09:57:05'),
(11, 'logout', 'Administrator melakukan logout.', '2018-08-10 09:57:08', '2018-08-10 09:57:08'),
(12, 'login', 'Administrator melakukan login.', '2018-08-10 09:57:37', '2018-08-10 09:57:37'),
(13, 'logout', 'Administrator melakukan logout.', '2018-08-10 09:57:49', '2018-08-10 09:57:49'),
(14, 'login', 'Administrator melakukan login.', '2018-08-10 09:58:48', '2018-08-10 09:58:48'),
(15, 'logout', 'Administrator melakukan logout.', '2018-08-10 09:58:59', '2018-08-10 09:58:59'),
(16, 'login', 'Administrator melakukan login.', '2018-08-10 10:05:36', '2018-08-10 10:05:36'),
(17, 'logout', 'Administrator melakukan logout.', '2018-08-10 10:06:11', '2018-08-10 10:06:11'),
(18, 'login', 'Administrator melakukan login.', '2018-08-23 06:08:29', '2018-08-23 06:08:29'),
(19, 'logout', 'Administrator melakukan logout.', '2018-08-23 06:41:52', '2018-08-23 06:41:52'),
(20, 'login', 'Administrator melakukan login.', '2018-12-13 10:03:47', '2018-12-13 10:03:47'),
(21, 'login', 'Administrator melakukan login.', '2018-12-13 11:23:25', '2018-12-13 11:23:25'),
(22, 'logout', 'Administrator melakukan logout.', '2018-12-13 11:35:54', '2018-12-13 11:35:54');

-- --------------------------------------------------------

--
-- Table structure for table `lokasis`
--

CREATE TABLE `lokasis` (
  `id` int(10) UNSIGNED NOT NULL,
  `sektor_id` int(10) UNSIGNED NOT NULL,
  `lokasi` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `akta_pendirian` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `rincian` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nama_personil` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cara_perolehan` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `lokasis`
--

INSERT INTO `lokasis` (`id`, `sektor_id`, `lokasi`, `akta_pendirian`, `rincian`, `nama_personil`, `cara_perolehan`, `created_at`, `updated_at`) VALUES
(1, 1, 'Kementerian Luar Negeri', NULL, NULL, NULL, NULL, '2018-07-31 21:33:50', '2018-08-05 21:30:04'),
(2, 1, 'Kementerian Kelautan dan Perikanan', NULL, NULL, NULL, NULL, '2018-07-31 21:33:50', '2018-07-31 21:33:50'),
(3, 2, 'Pemerintah Kabupaten Gorontalo', NULL, NULL, NULL, NULL, '2018-07-31 21:33:50', '2018-07-31 21:33:50'),
(4, 2, 'Pemerintah Kabupaten Mojokerto', NULL, NULL, NULL, NULL, '2018-07-31 21:33:50', '2018-07-31 21:33:50'),
(5, 3, 'PT. Pertamina (Pusat)', NULL, NULL, NULL, NULL, '2018-07-31 21:33:50', '2018-07-31 21:33:50'),
(6, 3, 'PT. Adhi Karya', NULL, NULL, NULL, NULL, '2018-07-31 21:33:50', '2018-07-31 21:33:50'),
(7, 4, 'Bank Mandiri KCP Jakarta Ragunan', NULL, NULL, NULL, NULL, '2018-07-31 21:33:50', '2018-07-31 21:33:50'),
(8, 4, 'Bank BCA KCP Jakarta Cilandak', NULL, NULL, NULL, NULL, '2018-07-31 21:33:50', '2018-07-31 21:33:50'),
(9, 5, 'Laboratorium Universitas Indonesia', NULL, NULL, NULL, NULL, '2018-07-31 21:33:50', '2018-07-31 21:33:50'),
(10, 5, 'Laboratorium Universitas Negeri Jakarta', NULL, NULL, NULL, NULL, '2018-07-31 21:33:50', '2018-07-31 21:33:50');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2018_07_31_071817_create_sektors_table', 1),
(4, '2018_07_31_073848_create_lokasis_table', 1),
(5, '2018_07_31_074613_create_produsens_table', 1),
(6, '2018_07_31_140509_create_jenis_barangs_table', 1),
(7, '2018_07_31_140653_create_kategoris_table', 1),
(8, '2018_07_31_140704_create_barangs_table', 1),
(9, '2018_07_31_140714_create_transaksis_table', 1),
(10, '2018_08_10_152541_create_logs_table', 2);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `produsens`
--

CREATE TABLE `produsens` (
  `id` int(10) UNSIGNED NOT NULL,
  `produsen` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nomor_izin` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sumber_permintaan` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cara_perolehan` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `produsens`
--

INSERT INTO `produsens` (`id`, `produsen`, `nomor_izin`, `sumber_permintaan`, `cara_perolehan`, `created_at`, `updated_at`) VALUES
(1, 'PT. Indoguardika Cipta Kreasi', NULL, NULL, NULL, '2018-07-31 21:33:50', '2018-08-05 21:07:52'),
(2, 'PT. Guna Multi Mandiri', NULL, NULL, NULL, '2018-07-31 21:33:50', '2018-07-31 21:33:50');

-- --------------------------------------------------------

--
-- Table structure for table `sektors`
--

CREATE TABLE `sektors` (
  `id` int(10) UNSIGNED NOT NULL,
  `sektor` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sektors`
--

INSERT INTO `sektors` (`id`, `sektor`, `created_at`, `updated_at`) VALUES
(1, 'Pemerintah Pusat', '2018-07-31 21:33:50', '2018-08-07 08:21:10'),
(2, 'Pemerintah Daerah', '2018-07-31 21:33:50', '2018-07-31 21:33:50'),
(3, 'BUMN', '2018-07-31 21:33:50', '2018-07-31 21:33:50'),
(4, 'Perbankan', '2018-07-31 21:33:50', '2018-07-31 21:33:50'),
(5, 'Pendidikan', '2018-07-31 21:33:50', '2018-07-31 21:33:50'),
(6, 'Swasta', '2018-08-07 08:21:44', '2018-08-07 08:21:44');

-- --------------------------------------------------------

--
-- Table structure for table `transaksis`
--

CREATE TABLE `transaksis` (
  `id` int(10) UNSIGNED NOT NULL,
  `lokasi_id` int(10) UNSIGNED NOT NULL,
  `barang_id` int(10) UNSIGNED NOT NULL,
  `jml_barang` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `username` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'Administrator', 'administrator@bssn.go.id', '$2y$12$j6oSb8hSp0tQRhFdM8FndeFoUKdXsZ2o7NO8fCYiXDi0sDerpJpTG', 'JZtD4JeFWOLjvPgK5v0nvPolt6wsZLVuiGUTDiVnBM5J2xUs7i8V7cZ3QFXL', '2018-08-10 01:19:40', '2018-08-10 08:10:26');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `barangs`
--
ALTER TABLE `barangs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `barangs_kategori_id_foreign` (`kategori_id`),
  ADD KEY `barangs_produsen_id_foreign` (`produsen_id`);

--
-- Indexes for table `jenis_barangs`
--
ALTER TABLE `jenis_barangs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kategoris`
--
ALTER TABLE `kategoris`
  ADD PRIMARY KEY (`id`),
  ADD KEY `kategoris_jenis_id_foreign` (`jenis_id`);

--
-- Indexes for table `logs`
--
ALTER TABLE `logs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lokasis`
--
ALTER TABLE `lokasis`
  ADD PRIMARY KEY (`id`),
  ADD KEY `lokasis_sektor_id_foreign` (`sektor_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `produsens`
--
ALTER TABLE `produsens`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sektors`
--
ALTER TABLE `sektors`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `transaksis`
--
ALTER TABLE `transaksis`
  ADD PRIMARY KEY (`id`),
  ADD KEY `transaksis_lokasi_id_foreign` (`lokasi_id`),
  ADD KEY `transaksis_barang_id_foreign` (`barang_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `barangs`
--
ALTER TABLE `barangs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `jenis_barangs`
--
ALTER TABLE `jenis_barangs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `kategoris`
--
ALTER TABLE `kategoris`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `logs`
--
ALTER TABLE `logs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `lokasis`
--
ALTER TABLE `lokasis`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `produsens`
--
ALTER TABLE `produsens`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `sektors`
--
ALTER TABLE `sektors`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `transaksis`
--
ALTER TABLE `transaksis`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `barangs`
--
ALTER TABLE `barangs`
  ADD CONSTRAINT `barangs_kategori_id_foreign` FOREIGN KEY (`kategori_id`) REFERENCES `kategoris` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `barangs_produsen_id_foreign` FOREIGN KEY (`produsen_id`) REFERENCES `produsens` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `kategoris`
--
ALTER TABLE `kategoris`
  ADD CONSTRAINT `kategoris_jenis_id_foreign` FOREIGN KEY (`jenis_id`) REFERENCES `jenis_barangs` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `lokasis`
--
ALTER TABLE `lokasis`
  ADD CONSTRAINT `lokasis_sektor_id_foreign` FOREIGN KEY (`sektor_id`) REFERENCES `sektors` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `transaksis`
--
ALTER TABLE `transaksis`
  ADD CONSTRAINT `transaksis_barang_id_foreign` FOREIGN KEY (`barang_id`) REFERENCES `barangs` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `transaksis_lokasi_id_foreign` FOREIGN KEY (`lokasi_id`) REFERENCES `lokasis` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
