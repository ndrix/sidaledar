@extends('template')

@section('content')
    <div class="page-content">
        <!-- BEGIN BREADCRUMBS -->
        <div class="breadcrumbs">
            <h1>Kategori Perangkat</h1>
            <ol class="breadcrumb">
                <li>
                    <a href="{{ route('home') }}">Home</a>
                </li>
                <li>
                    <a href="#">Data</a>
                </li>
                <li class="active">
                    Perangkat
                </li>
            </ol>
        </div>
        <!-- END BREADCRUMBS -->
        <!-- BEGIN CONTENT -->
        @if ($message = Session::get('success'))
            <div class="alert alert-success alert-dismissable">
                <p>{{ $message }}</p>
            </div>
        @endif
        @if ($message = Session::get('error'))
            <div class="alert alert-danger alert-dismissable">
                <p>{{ $message }}</p>
            </div>
        @endif
        @if ($errors->any())
            <div class="alert alert-danger">
                <strong>Whoops!</strong> Something wrong with your inputs.<br><br>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <div class="row">
            <form action="{{ url()->current() }}">
                <div class="col-md-4">
                    @if($keyword != null)
                        <input type="text" class="form-control" name="search" placeholder="Cari..." value="{{ $keyword }}">
                    @else
                        <input type="text" class="form-control" name="search" placeholder="Cari...">
                    @endif
                </div>
                <div class="col-md-2">
                    <button type="submit" class="btn blue">Cari</button>
                    @php $route_name = \Request::route()->getName(); @endphp
                    <a href="{{ route($route_name) }}" class="btn default">Reset</a>
                </div>
            </form>
        </div><br>
        @if ($keyword != NULL)
            <div class="row">
                <div class="col-md-12">
                    <div class="alert alert-info">
                        <p>Menampilkan hasil pencarian untuk <b>"{{ $keyword }}"</b></p>
                    </div>
                </div>
            </div>
        @endif
        <div class="row">
            <div class="col-md-12">
                <div class="table-scrollable">
                    <table class="table table-striped table-bordered table-hover">
                        <tr>
                            <th width="5%"><center>No</center></th>
                            <th width="68%"><center>Kategori</center></th>
                            <th width="20%"><center>Jenis</center></th>
                            <th width="7%"></th>
                        </tr>
                        @foreach($kategori as $k)
                            <tr>
                                <td align="center">{{ $loop->iteration }}</td>
                                <td>{{ $k->kategori }}</td>
                                <td><center>{{ $k->jenis->jenis }}</center></td>
                                <form method="post" action="{{ route('kategori.delete', ['kategori' => $k->id]) }}">
                                    @csrf @method('delete')
                                    <td>
                                        <center>
                                        <a href="{{ route('kategori.edit', ['kategori' => $k->id]) }}" class="btn blue btn-xs"><i class="fa fa-pencil"></i></a>
                                        <button type="submit" class="btn red btn-xs" onclick="return confirm('Anda yakin ingin menghapus entri ini?');"><i class="fa fa-trash"></i></button>
                                        </center>
                                    </td>
                                </form>
                            </tr>
                        @endforeach
                    </table>
                </div>
            </div>
        </div>
        <!-- END CONTENT -->
    </div>
@endsection