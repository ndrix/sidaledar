@extends('template')

@section('content')
    <div class="page-content">
        <!-- BEGIN BREADCRUMBS -->
        <div class="breadcrumbs">
            <h1>Perangkat Baru</h1>
            <ol class="breadcrumb">
                <li>
                    <a href="{{ route('home') }}">Home</a>
                </li>
                <li>
                    <a href="">Data</a>
                </li>
                <li class="active">
                    Perangkat
                </li>
            </ol>
        </div>
        <!-- END BREADCRUMBS -->
        <!-- BEGIN CONTENT -->
        @if ($message = Session::get('success'))
            <div class="alert alert-success alert-dismissable">
                <p>{{ $message }}</p>
            </div>
        @endif
        @if ($message = Session::get('error'))
            <div class="alert alert-danger alert-dismissable">
                <p>{{ $message }}</p>
            </div>
        @endif
        @if ($errors->any())
            <div class="alert alert-danger">
                <strong>Whoops!</strong> Something wrong with your inputs.<br><br>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <div class="row">
            <div class="col-md-12">
                <form action="{{ route('barang.store') }}" method="post">
                    @csrf
                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label">Merk</label>
                            <input type="text" class="form-control" name="merk">
                        </div>
                        <div class="form-group">
                            <label class="control-label">Tipe</label>
                            <input type="text" class="form-control" name="tipe">
                        </div>
                        <div class="form-group">
                            <label class="control-label">Kategori</label>
                            <select name="kategori_id" class="form-control select2">
                                <option value=""></option>
                                @foreach($kategori as $k)
                                    <option value="{{ $k->id }}">{{ $k->kategori }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Produsen</label>
                            <select name="produsen_id" class="form-control select2">
                                <option value=""></option>
                                @foreach($produsen as $p)
                                    <option value="{{ $p->id }}">{{ $p->produsen }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Keterangan</label>
                            <select name="keterangan" class="bs-select form-control">
                                <option value="belum">Belum Disertifikasi</option>
                                <option value="sudah">Sudah Disertifikasi</option>
                            </select>
                        </div>
                    </div><hr>
                    <div class="form-actions">
                        <center>
                            <button type="submit" class="btn green">Simpan</button>
                            <a href="{{ URL::previous()  }}" class="btn default">Batal</a>
                        </center>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection