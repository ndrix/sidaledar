<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Log;

class LogController extends Controller
{
    public function index(Request $request)
    {
        $keyword                    = $request->get('search');

        //Fungsi search di halaman index
        if($keyword != null){
            $logs                   = Log::where('jenis', 'like', '%'.$keyword.'%')
                                        ->orWhere('log', 'like', '%'.$keyword.'%')
                                        ->get();
        } else {
            $logs                   = Log::all();
        }

        return view('log.index', ['logs' => $logs, 'keyword' => $keyword]);
    }
}
