<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Barang;
use App\Kategori;
use App\Produsen;
use App\Http\Requests\BarangRequest;
use Auth;

class BarangController extends Controller
{
    public function index(Request $request)
    {
        $keyword                    = $request->get('search');
        $barang                     = Barang::all();

        //Fungsi search di halaman index
        if($keyword != null){
            $barang                 = Barang::whereHas('kategori', function($query) use($keyword) { $query->where('kategori', 'like', '%'.$keyword.'%'); })
                                        ->orWhere('merk', 'like', '%'.$keyword.'%')
                                        ->orWhere('tipe', 'like', '%'.$keyword.'%')
                                        ->orWhereHas('produsen', function($query) use($keyword) { $query->where('produsen', 'like', '%'.$keyword.'%'); })
                                        ->orWhere('keterangan', 'like', '%'.$keyword.'%')
                                        ->get();
        }

        return view('barang.index', ['barang' => $barang, 'keyword' => $keyword]);
    }
    
    public function create()
    {
        $kategori                   = Kategori::all();
        $produsen                   = Produsen::all();

        return view('barang.create', ['kategori' => $kategori, 'produsen' => $produsen]);
    }

    public function store(BarangRequest $request)
    {
        $barang['kategori_id']      = $request->get('kategori_id');
        $barang['produsen_id']         = $request->get('produsen_id');
        $barang['merk']             = $request->get('merk');
        $barang['tipe']             = $request->get('tipe');
        $barang['keterangan']       = $request->get('keterangan');
        Barang::create($barang);

        $log['jenis'] = "add";
        $log['log']   = Auth::user()->name." melakukan penambahan barang.";
        \App\Log::create($log);

        return redirect()->back()->with('success', 'Berhasil menambah perangkat baru.');
    }

    public function edit(Barang $barang)
    {
        $kategori                   = Kategori::all();
        $produsen                   = Produsen::all();

        return view('barang.edit', ['barang' => $barang, 'kategori' => $kategori, 'produsen' => $produsen]);
    }

    public function update(Barang $barang, BarangRequest $request)
    {
        $barang->merk               = $request->get('merk');
        $barang->tipe               = $request->get('tipe');
        $barang->kategori_id        = $request->get('kategori_id');
        $barang->produsen_id        = $request->get('produsen_id');
        $barang->keterangan         = $request->get('keterangan');
        $barang->save();

        $log['jenis'] = "update";
        $log['log']   = Auth::user()->name." melakukan pengubahan data barang.";
        \App\Log::create($log);

        return redirect()->back()->with('success', 'Berhasil mengubah data perangkat.');
    }

    public function destroy(Barang $barang)
    {
        $barang->delete();

        $log['jenis'] = "delete";
        $log['log']   = Auth::user()->name." melakukan penghapusan data barang.";
        \App\Log::create($log);

        return redirect()->back()->with('success', 'Berhasil menghapus data perangkat.');
    }
}
