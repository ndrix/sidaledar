<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class SektorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('sektors')->insert(array(
            array(
                'sektor'        => 'Pemerintah Pusat',
                'created_at'    => Carbon::now(),
                'updated_at'    => Carbon::now(),
            ),
            array(
                'sektor'        => 'Pemerintah Daerah',
                'created_at'    => Carbon::now(),
                'updated_at'    => Carbon::now(),
            ),
            array(
                'sektor'        => 'BUMN',
                'created_at'    => Carbon::now(),
                'updated_at'    => Carbon::now(),
            ),
            array(
                'sektor'        => 'Perbankan',
                'created_at'    => Carbon::now(),
                'updated_at'    => Carbon::now(),
            ),
            array(
                'sektor'        => 'Pendidikan',
                'created_at'    => Carbon::now(),
                'updated_at'    => Carbon::now(),
            ),
            array(
                'sektor'        => 'Swasta',
                'created_at'    => Carbon::now(),
                'updated_at'    => Carbon::now(),
            ),
        ));
    }
}
