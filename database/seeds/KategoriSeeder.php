<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class KategoriSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('kategoris')->insert(array(
            array(
                'jenis_id'      => 1,
                'kategori'      => 'Modul Kriptografi Hardware',
                'created_at'    => Carbon::now(),
                'updated_at'    => Carbon::now(),
            ),
            array(
                'jenis_id'      => 1,
                'kategori'      => 'Modul Kriptografi Software',
                'created_at'    => Carbon::now(),
                'updated_at'    => Carbon::now(),
            ),
            array(
                'jenis_id'      => 1,
                'kategori'      => 'Modul Kriptografi Firmware',
                'created_at'    => Carbon::now(),
                'updated_at'    => Carbon::now(),
            ),
            array(
                'jenis_id'      => 1,
                'kategori'      => 'Modul Kriptografi Software Hybrid',
                'created_at'    => Carbon::now(),
                'updated_at'    => Carbon::now(),
            ),
            array(
                'jenis_id'      => 1,
                'kategori'      => 'Modul Kriptografi Firmware Hybrid',
                'created_at'    => Carbon::now(),
                'updated_at'    => Carbon::now(),
            ),
            array(
                'jenis_id'      => 2,
                'kategori'      => 'Access Control and System - Identify Management',
                'created_at'    => Carbon::now(),
                'updated_at'    => Carbon::now(),
            ),
            array(
                'jenis_id'      => 2,
                'kategori'      => 'Access Control and System - Authentication System',
                'created_at'    => Carbon::now(),
                'updated_at'    => Carbon::now(),
            ),
            array(
                'jenis_id'      => 2,
                'kategori'      => 'Key Management System - Active Directory',
                'created_at'    => Carbon::now(),
                'updated_at'    => Carbon::now(),
            ),
            array(
                'jenis_id'      => 2,
                'kategori'      => 'Key Management System - Configuration Management System',
                'created_at'    => Carbon::now(),
                'updated_at'    => Carbon::now(),
            ),
            array(
                'jenis_id'      => 2,
                'kategori'      => 'Key Management System - Public Key Infrastructure',
                'created_at'    => Carbon::now(),
                'updated_at'    => Carbon::now(),
            ),
            array(
                'jenis_id'      => 2,
                'kategori'      => 'Key Management System - Digital Certificate',
                'created_at'    => Carbon::now(),
                'updated_at'    => Carbon::now(),
            ),
            array(
                'jenis_id'      => 2,
                'kategori'      => 'Key Management System - Passport System',
                'created_at'    => Carbon::now(),
                'updated_at'    => Carbon::now(),
            ),
            array(
                'jenis_id'      => 2,
                'kategori'      => 'Network System and Device - VPN, Proxy, Firewall, Router',
                'created_at'    => Carbon::now(),
                'updated_at'    => Carbon::now(),
            ),
            array(
                'jenis_id'      => 2,
                'kategori'      => 'Network System and Device - Unified Threat Management',
                'created_at'    => Carbon::now(),
                'updated_at'    => Carbon::now(),
            ),
            array(
                'jenis_id'      => 2,
                'kategori'      => 'Network System and Device - Network Performance System',
                'created_at'    => Carbon::now(),
                'updated_at'    => Carbon::now(),
            ),
            array(
                'jenis_id'      => 2,
                'kategori'      => 'Network System and Device - Switch',
                'created_at'    => Carbon::now(),
                'updated_at'    => Carbon::now(),
            ),
            array(
                'jenis_id'      => 2,
                'kategori'      => 'Network System and Device - Intrusion Detection System',
                'created_at'    => Carbon::now(),
                'updated_at'    => Carbon::now(),
            ),
            array(
                'jenis_id'      => 2,
                'kategori'      => 'Network System and Device - Intrusion Prevention System',
                'created_at'    => Carbon::now(),
                'updated_at'    => Carbon::now(),
            ),
            array(
                'jenis_id'      => 2,
                'kategori'      => 'Data Protection - Data and File Encryption',
                'created_at'    => Carbon::now(),
                'updated_at'    => Carbon::now(),
            ),
            array(
                'jenis_id'      => 2,
                'kategori'      => 'Data Protection - Securing Data Detection',
                'created_at'    => Carbon::now(),
                'updated_at'    => Carbon::now(),
            ),
            array(
                'jenis_id'      => 2,
                'kategori'      => 'Wireless and Mobile Devices System - Access Point Management System',
                'created_at'    => Carbon::now(),
                'updated_at'    => Carbon::now(),
            ),
            array(
                'jenis_id'      => 2,
                'kategori'      => 'Wireless and Mobile Device System - Access id/key encryption (wired equvalent privacy(WEP)/wi-fi protection access(WPA))',
                'created_at'    => Carbon::now(),
                'updated_at'    => Carbon::now(),
            ),
            array(
                'jenis_id'      => 2,
                'kategori'      => 'Smart Card and Related Device - Smart Card Chip',
                'created_at'    => Carbon::now(),
                'updated_at'    => Carbon::now(),
            ),
            array(
                'jenis_id'      => 2,
                'kategori'      => 'Smart Card and Related Device - Smart Card Applet',
                'created_at'    => Carbon::now(),
                'updated_at'    => Carbon::now(),
            ),
            array(
                'jenis_id'      => 2,
                'kategori'      => 'Smart Card and Related Device - Smart Card Reader',
                'created_at'    => Carbon::now(),
                'updated_at'    => Carbon::now(),
            ),
            array(
                'jenis_id'      => 2,
                'kategori'      => 'Smart Card and Related Device - Smart Card Operating System',
                'created_at'    => Carbon::now(),
                'updated_at'    => Carbon::now(),
            ),
        ));
    }
}
