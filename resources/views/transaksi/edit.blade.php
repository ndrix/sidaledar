@extends('template')

@section('content')
    <div class="page-content">
        <!-- BEGIN BREADCRUMBS -->
        <div class="breadcrumbs">
            <h1>Ubah Data Transaksi</h1>
            <ol class="breadcrumb">
                <li>
                    <a href="{{ route('home') }}">Home</a>
                </li>
                <li>
                    <a href="">Data</a>
                </li>
                <li class="active">
                    Sektor
                </li>
            </ol>
        </div>
        <!-- END BREADCRUMBS -->
        <!-- BEGIN CONTENT -->
        @if ($message = Session::get('success'))
            <div class="alert alert-success alert-dismissable">
                <p>{{ $message }}</p>
            </div>
        @endif
        @if ($message = Session::get('error'))
            <div class="alert alert-danger alert-dismissable">
                <p>{{ $message }}</p>
            </div>
        @endif
        @if ($errors->any())
            <div class="alert alert-danger">
                <strong>Whoops!</strong> Something wrong with your inputs.<br><br>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <div class="row">
            <div class="col-md-12">
            <form action="{{ route('transaksi.update', ['transaksi' => $transaksi->id]) }}" method="post">
                @csrf @method('put')
                <div class="form-body">
                    <div class="form-group">
                        <label class="control-label">Lokasi</label>
                        <select class="form-control select2" name="lokasi_id">
                            <option value=""></option>
                            @foreach($lokasi as $l)
                                @if($l->id == $transaksi->lokasi_id)
                                    <option value="{{ $l->id }}" selected>{{ $l->lokasi }}</option>
                                @else
                                    <option value="{{ $l->id }}">{{ $l->lokasi }}</option>
                                @endif
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Barang</label>
                        <select name="barang_id" class="form-control select2">
                            <option value=""></option>
                            @foreach($barang as $b)
                                @if($b->id == $transaksi->barang_id)
                                    <option value="{{ $b->id }}" selected>{{ ucwords($b->merk." ".$b->tipe) }} ( {{ $b->produsen->produsen }} )</option>
                                @else
                                    <option value="{{ $b->id }}">{{ ucwords($b->merk." ".$b->tipe) }} ( {{ $b->produsen->produsen }} )</option>
                                @endif
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Jumlah</label>
                        <input type="text" class="form-control" name="jumlah" value="{{ $transaksi->jml_barang }}">
                    </div>
                </div><hr>
                <div class="form-actions">
                    <center>
                        <button type="submit" class="btn green">Simpan</button>
                        <a href="{{ URL::previous()  }}" class="btn default">Batal</a>
                    </center>
                </div>
            </form>
            </div>
        </div>
    </div>
@endsection