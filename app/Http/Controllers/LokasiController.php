<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Lokasi;
use App\Sektor;
use App\Http\Requests\LokasiRequest;
use Auth;

class LokasiController extends Controller
{
    public function index(Request $request)
    {
        $keyword                    = $request->get('search');

        //Fungsi search di halaman index
        if($keyword != null){
            $lokasi                 = Lokasi::where('lokasi', 'like', '%'.$keyword.'%')
                                        ->get();
        } else {
            $lokasi                 = Lokasi::all();
        }

        return view('lokasi.index', ['lokasi' => $lokasi, 'keyword' => $keyword]);
    }

    public function create()
    {
        $sektor                     = Sektor::all();

        return view('lokasi.create', ['sektor' => $sektor]);
    }

    public function store(LokasiRequest $request)
    {
        $lokasi['lokasi']           = $request->get('nama');
        $lokasi['sektor_id']        = $request->get('sektor_id');
        Lokasi::create($lokasi);

        $log['jenis'] = "add";
        $log['log']   = Auth::user()->name." melakukan penambahan lokasi.";
        \App\Log::create($log);

        return redirect()->back()->with('success', 'Berhasil menambahkan lokasi baru.');
    }

    public function edit(Lokasi $lokasi)
    {
        $sektor                     = Sektor::all();
        return view('lokasi.edit', ['lokasi' => $lokasi, 'sektor' => $sektor]);
    }

    public function update(Lokasi $lokasi, LokasiRequest $request)
    {
        $lokasi->lokasi             = $request->get('nama');
        $lokasi->sektor_id          = $request->get('sektor_id');
        $lokasi->save();

        $log['jenis'] = "update";
        $log['log']   = Auth::user()->name." melakukan pengubahan data lokasi.";
        \App\Log::create($log);

        return redirect()->back()->with('success', 'Berhasil mengubah data lokasi.');
    }

    public function destroy(Lokasi $lokasi)
    {
        $lokasi->delete();

        $log['jenis'] = "delete";
        $log['log']   = Auth::user()->name." melakukan penghapusan data lokasi.";
        \App\Log::create($log);

        return redirect()->back()->with('success', 'Berhasil menghapus data lokasi.');
    }
}
