<!DOCTYPE html>
<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->
    <head>
        <meta charset="utf-8" />
        <title>SIPeDATI</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="Design by Septian Eko N, Code by Hendrik Maulana" name="author" />
        <!-- BEGIN LAYOUT FIRST STYLES -->
        <link href="{{ asset('global/plugins/font-awesome/css/font-googleapis-oswald.css') }}" rel="stylesheet" type="text/css" />
        <!-- END LAYOUT FIRST STYLES -->
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="{{ asset('global/plugins/font-awesome/css/font-googleapis-opensans.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('global/plugins/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('global/plugins/simple-line-icons/simple-line-icons.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('global/plugins/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('global/plugins/bootstrap-switch/css/bootstrap-switch.min.css') }}" rel="stylesheet" type="text/css" />
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <link href="{{ asset('global/plugins/bootstrap-daterangepicker/daterangepicker.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('global/plugins/morris/morris.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('global/plugins/select2/css/select2.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('global/plugins/select2/css/select2-bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('global/plugins/bootstrap-select/css/bootstrap-select.css') }}" rel="stylesheet" type="text/css" />
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL STYLES -->
        <link href="{{ asset('global/css/components.min.css') }}" rel="stylesheet" id="style_components" type="text/css" />
        <link href="{{ asset('global/css/plugins.min.css') }}" rel="stylesheet" type="text/css" />
        <!-- END THEME GLOBAL STYLES -->
        <!-- BEGIN THEME LAYOUT STYLES -->
        <link href="{{ asset('layouts/layout5/css/layout.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('layouts/layout5/css/custom.min.css') }}" rel="stylesheet" type="text/css" />
        <!-- END THEME LAYOUT STYLES -->
        <link rel="shortcut icon" href="favicon.ico" />
    </head>
    <!-- END HEAD -->
    <!-- BEGIN BODY -->
    <body class="page-header-fixed page-sidebar-closed-hide-logo">
        <!-- BEGIN CONTAINER -->
        <div class="wrapper">
            <!-- BEGIN HEADER -->
            <header class="page-header">
                <nav class="navbar mega-menu" role="navigation">
                    <div class="container-fluid">
                        <div class="clearfix navbar-fixed-top">
                            <!-- Brand and toggle get grouped for better mobile display -->
                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-responsive-collapse">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="toggle-icon">
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </span>
                            </button>
                            <!-- End Toggle Button -->
                            <!-- BEGIN LOGO -->
                            <a id="index" class="page-logo" href="{{ url('/') }}">
                                <img src="{{ asset('layouts/layout5/img/logo.png') }}" alt="Logo"> 
                            </a>
                            <!-- END LOGO -->
                            <!-- BEGIN TOPBAR ACTIONS -->
                            <div class="topbar-actions">
                                <!-- BEGIN GROUP INFORMATION -->
                                <div class="btn-group-red btn-group">
                                    <button type="button" class="btn btn-sm md-skip dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                        <i class="fa fa-plus"></i>
                                    </button>
                                    <ul class="dropdown-menu-v2" role="menu">
                                        <li>
                                            <a href="{{ route('sektor.create') }}">Sektor Baru</a>
                                        </li>
                                        <li>
                                            <a href="{{ route('lokasi.create') }}">Lokasi Baru</a>
                                        </li>
                                        <li>
                                            <a href="{{ route('kategori.create')}} ">Kategori Baru</a>
                                        </li>
                                        <li>
                                            <a href="{{ route('barang.create') }}">Perangkat Baru</a>
                                        </li>
                                        <li>
                                            <a href="{{ route('produsen.create') }}">Produsen Baru</a>
                                        </li>
                                        <li>
                                            <a href="{{ route('transaksi.create') }}">Transaksi Baru</a>
                                        </li>
                                        <li>
                                            <a href="{{ route('user.create') }}">User Baru</a>
                                        </li>
                                        {{-- <li class="divider"></li> --}}
                                    </ul>
                                </div>
                                <!-- END GROUP INFORMATION -->
                                <!-- BEGIN USER PROFILE -->
                                <div class="btn-group-img btn-group">
                                    <button type="button" class="btn btn-sm md-skip dropdown-toggle">
                                        <span>Hi, {{Auth::user()->name}}</span>
                                        <img src="{{ asset('layouts/layout5/img/avatar1.jpg') }}" alt="">
                                    </button>
                                </div>
                                <!-- END USER PROFILE -->
                                <!-- BEGIN QUICK SIDEBAR TOGGLER -->
                                <a href="{{ route('logout') }}" onclick="event.preventDefault();
                                    document.getElementById('logout-form').submit();"
                                    {{ __('Logout') }}>
                                    <button type="button" class="quick-sidebar-toggler md-skip">
                                        <span class="sr-only">Logout</span>
                                        <i class="icon-logout"></i>
                                    </button>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </a>
                                <!-- END QUICK SIDEBAR TOGGLER -->
                            </div>
                            <!-- END TOPBAR ACTIONS -->
                        </div>
                        <!-- BEGIN HEADER MENU -->
                        <div class="nav-collapse collapse navbar-collapse navbar-responsive-collapse">
                            <ul class="nav navbar-nav">
                                @php 
                                    if(\Request::route()->getName() == "home" OR \Request::route()->getName() == "analisis"){ 
                                        echo '
                                            <li class="dropdown dropdown-fw dropdown-fw-disabled active open selected">
                                        '; 
                                    } else {
                                        echo '<li class="dropdown dropdown-fw dropdown-fw-disabled">';
                                    }
                                @endphp
                                    <a href="javascript:;" class="text-uppercase">
                                        <i class="icon-home"></i> Home </a>
                                    <ul class="dropdown-menu dropdown-menu-fw">
                                        @if(\Request::route()->getName() == "home")
                                        <li class="active">
                                        @else
                                        <li>
                                        @endif
                                            <a href="{{ route('home') }}">
                                                <i class="fa fa-home"></i> Dashboard </a>
                                        </li>
                                        @if(\Request::route()->getName() == "analisis")
                                        <li class="active">
                                        @else
                                        <li>
                                        @endif
                                            <a href="{{ route('analisis') }}">
                                                <i class="fa fa-calculator"></i> Analisis Data </a>
                                        </li>
                                    </ul>
                                </li>
                                @php 
                                    $route_name = \Request::route()->getName();
                                @endphp
                                @if(strpos($route_name,"sektor") !== false OR strpos($route_name,"jenis") !== false OR strpos($route_name,"produsen") !== false OR strpos($route_name,"lokasi") !== false OR strpos($route_name,"barang") !== false OR strpos($route_name,"transaksi") !== false OR strpos($route_name,"kategori") !== false)
                                <li class="dropdown dropdown-fw dropdown-fw-disabled active open selected">
                                @else
                                <li class="dropdown dropdown-fw dropdown-fw-disabled">
                                @endif
                                    <a href="javascript:;" class="text-uppercase">
                                        <i class="fa fa-database"></i> Data </a>
                                    <ul class="dropdown-menu dropdown-menu-fw">
                                        @if(strpos($route_name,"sektor") !== false OR strpos($route_name,"transaksi") !== false)
                                        <li class="dropdown more-dropdown-sub active">
                                        @else
                                        <li class="dropdown more-dropdown-sub">
                                        @endif
                                            <a href="javascript:;">
                                                <i class="fa fa-institution"></i> Sektor </a>
                                            <ul class="dropdown-menu">
                                                <li>
                                                    <a href="{{ route('sektor.index') }}"> All </a>
                                                </li>
                                                @php
                                                    $sektor_all         = \App\Sektor::all();
                                                @endphp
                                                @foreach($sektor_all as $n)
                                                    <li>
                                                        <a href="{{ route('sektor.show', ['id' => $n->id]) }}"> {{ ucwords($n->sektor) }} </a>
                                                    </li>                                                    
                                                @endforeach
                                                <li class="divider"></li>
                                                <li>
                                                    <a href="{{ route('sektor.list') }}"> Daftar Sektor </a>
                                                </li>
                                            </ul>
                                        </li>
                                        @if(strpos($route_name,"jenis") !== false OR strpos($route_name,"barang") !== false OR strpos($route_name,"kategori") !== false)
                                        <li class="dropdown more-dropdown-sub active">
                                        @else
                                        <li class="dropdown more-dropdown-sub">
                                        @endif
                                            <a href="javascript:;">
                                                <i class="fa fa-tasks"></i> Perangkat </a>
                                            <ul class="dropdown-menu">
                                                <li>
                                                    <a href="{{ route('barang.index') }}"> All </a>
                                                </li>
                                                @php
                                                    $jenis_all         = \App\JenisBarang::all();
                                                @endphp
                                                @foreach($jenis_all as $n)
                                                    <li>
                                                        <a href="{{ route('jenis.show', ['id' => $n->id]) }}"> {{ ucwords($n->jenis) }} </a>
                                                    </li>                                                    
                                                @endforeach
                                                <li>
                                                    <a href="{{ route('kategori.index') }}"> Kategori Perangkat </a>
                                                </li>
                                            </ul>
                                        </li>
                                        @if(strpos($route_name,"produsen") !== false)
                                        <li class="dropdown more-dropdown-sub active">
                                        @else
                                        <li class="dropdown more-dropdown-sub">
                                        @endif
                                            <a href="javascript:;">
                                                <i class="fa fa-users"></i> Produsen </a>
                                            <ul class="dropdown-menu">
                                                <li>
                                                    <a href="{{ route('produsen.index') }}"> All </a>
                                                </li>
                                                @php
                                                    $jenis_all         = \App\JenisBarang::all();
                                                @endphp
                                                @foreach($jenis_all as $n)
                                                    <li>
                                                        <a href="{{ route('produsen.show', ['id' => $n->id]) }}"> Produsen {{ ucwords($n->jenis) }} </a>
                                                    </li>                                                    
                                                @endforeach
                                            </ul>
                                        </li>
                                        @if(strpos($route_name,"lokasi") !== false)
                                        <li class="active">
                                        @else
                                        <li>
                                        @endif
                                            <a href="{{ route('lokasi.index') }}">
                                                <i class="fa fa-map-marker"></i> Lokasi </a>
                                        </li>
                                    </ul>
                                </li>
                                @if(strpos($route_name,"user") !== false OR strpos($route_name,"log") !== false)
                                    <li class="dropdown dropdown-fw dropdown-fw-disabled active open selected">
                                @else
                                    <li class="dropdown dropdown-fw dropdown-fw-disabled"> 
                                @endif 
                                    <a href="javascript:;" class="text-uppercase"><i class="fa fa-gears"></i> Management </a>
                                    <ul class="dropdown-menu dropdown-menu-fw">
                                        @if(strpos($route_name,"user") !== false)
                                            <li class="active">
                                        @else
                                            <li>
                                        @endif
                                            <a href="{{ route('user.index') }}"><i class="fa fa-user"></i> User</a>
                                        </li>
                                        @if(strpos($route_name,"log") !== false)
                                            <li class="active">
                                        @else
                                            <li>
                                        @endif
                                            <a href="{{ route('log.index') }}"><i class="fa fa-exclamation-triangle"></i> Logs</a>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <!-- END HEADER MENU -->
                    </div>
                    <!--/container-->
                </nav>
            </header>
            <!-- END HEADER -->
            <!-- BEGIN CONTENT -->
            <div class="container-fluid">
                @yield('content')
                <!-- BEGIN FOOTER -->
                <p class="copyright"> 2018 &copy;
                    Badan Siber dan Sandi Negara
                </p>
                <a href="" class="go2top">
                    <i class="icon-arrow-up"></i>
                </a>
                <!-- END FOOTER -->
            </div>
            <!-- END CONTENT -->
        </div>
        <!-- END CONTAINER -->
                <!--[if lt IE 9]>
        <script src="../assets/global/plugins/respond.min.js"></script>
        <script src="../assets/global/plugins/excanvas.min.js"></script>
        <script src="../assets/global/plugins/ie8.fix.min.js"></script>
        <![endif]-->
        <!-- BEGIN CORE PLUGINS -->
        <script src="{{ asset('global/plugins/jquery.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('global/plugins/bootstrap/js/bootstrap.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('global/plugins/js.cookie.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('global/plugins/jquery-slimscroll/jquery.slimscroll.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('global/plugins/jquery.blockui.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('global/plugins/bootstrap-switch/js/bootstrap-switch.min.js') }}" type="text/javascript"></script>
        <!-- END CORE PLUGINS -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <script src="{{ asset('global/plugins/moment.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('global/plugins/bootstrap-daterangepicker/daterangepicker.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('global/plugins/morris/morris.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('global/plugins/morris/raphael-min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('global/plugins/counterup/jquery.waypoints.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('global/plugins/counterup/jquery.counterup.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('global/plugins/horizontal-timeline/horizontal-timeline.js') }}" type="text/javascript"></script>
        <script src="{{ asset('global/plugins/flot/jquery.flot.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('global/plugins/flot/jquery.flot.resize.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('global/plugins/flot/jquery.flot.categories.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('global/plugins/jquery-easypiechart/jquery.easypiechart.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('global/plugins/jquery.sparkline.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('global/plugins/select2/js/select2.full.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('global/plugins/bootstrap-select/js/bootstrap-select.min.js') }}" type="text/javascript"></script>
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL SCRIPTS -->
        <script src="{{ asset('global/scripts/app.min.js') }}" type="text/javascript"></script>
        <!-- END THEME GLOBAL SCRIPTS -->
        <!-- BEGIN PAGE LEVEL SCRIPTS -->
        <script src="{{ asset('pages/scripts/dashboard.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('pages/scripts/components-select2.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('pages/scripts/components-bootstrap-select.min.js') }}" type="text/javascript"></script>
        <!-- END PAGE LEVEL SCRIPTS -->
        <!-- BEGIN THEME LAYOUT SCRIPTS -->
        <script src="{{ asset('layouts/layout5/scripts/layout.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('layouts/global/scripts/quick-sidebar.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('layouts/global/scripts/quick-nav.min.js') }}" type="text/javascript"></script>
        @yield('scripts')
        <!-- END THEME LAYOUT SCRIPTS -->
        <script>
            $(document).ready(function()
            {
                $('#clickmewow').click(function()
                {
                    $('#radio1003').attr('checked', 'checked');
                });
            })
        </script>
    </body>