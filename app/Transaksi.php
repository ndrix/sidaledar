<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaksi extends Model
{
    protected $table = 'transaksis';

    protected $fillable = [
        'lokasi_id', 'barang_id', 'jml_barang'
    ];

    public function lokasi()
    {
        return $this->belongsTo('App\Lokasi', 'lokasi_id');
    }

    public function barang()
    {
        return $this->belongsTo('App\Barang', 'barang_id');
    }
}
