<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sektor extends Model
{
    protected $table = 'sektors';

    protected $fillable = [
        'sektor'
    ];

    public function lokasi()
    {
        return $this->hasMany('App\Lokasi', 'sektor_id');
    }
}
