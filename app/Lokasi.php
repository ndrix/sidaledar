<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Lokasi extends Model
{
    protected $table = 'lokasis';

    protected $fillable = [
        'sektor_id', 'lokasi', 'rincian'
    ];

    public function sektor()
    {
        return $this->belongsTo('App\Sektor', 'sektor_id');
    }

    public function transaksi()
    {
        return $this->hasMany('App\Transaksi', 'lokasi_id');
    }
}
