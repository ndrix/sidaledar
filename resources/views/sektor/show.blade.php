@extends('template')

@section('content')
    <div class="page-content">
        <!-- BEGIN BREADCRUMBS -->
        <div class="breadcrumbs">
            <h1>Sektor {{ ucwords($sektor['sektor']) }}</h1>
            <ol class="breadcrumb">
                <li>
                    <a href="{{ route('home') }}">Home</a>
                </li>
                <li>
                    <a href="">Data</a>
                </li>
                <li class="active">
                    Sektor
                </li>
            </ol>
        </div>
        <!-- END BREADCRUMBS -->
        <!-- BEGIN CONTENT -->
        <div class="row">
            <form action="{{ url()->current() }}">
                <div class="col-md-4">
                    @if($keyword != null)
                        <input type="text" class="form-control" name="search" placeholder="Cari..." value="{{ $keyword }}">
                    @else
                        <input type="text" class="form-control" name="search" placeholder="Cari...">
                    @endif
                </div>
                <div class="col-md-2">
                    <button type="submit" class="btn blue">Cari</button>
                    @php $route_name = \Request::route()->getName(); @endphp
                    <a href="{{ route($route_name, ['id' => $sektor['id']]) }}" class="btn default">Reset</a>
                </div>
            </form>
        </div><br>
        @if ($keyword != NULL)
            <div class="row">
                <div class="col-md-12">
                    <div class="alert alert-info">
                        <p>Menampilkan hasil pencarian untuk <b>"{{ $keyword }}"</b></p>
                    </div>
                </div>
            </div>
        @endif
        <div class="row">
            <div class="col-md-12">
                <div class="table-scrollable">
                    <table class="table table-striped table-bordered table-hover">
                        <tr>
                            <th width="5%"><center>No</center></th>
                            <th width="20%"><center>Lokasi</center></th>
                            <th width="10%"><center>Kategori</center></th>
                            <th width="13%"><center>Merk</center></th>
                            <th width="10%"><center>Tipe</center></th>
                            <th width="5%"><center>Jml</center></th>
                            <th width="20%"><center>Produsen</center></th>
                            <th width="10%"><center>Keterangan</center></th>
                            <th width="7%"></th>
                        </tr>
                        @php $i = 0; @endphp
                        @foreach($transaksi as $t)
                            @if($t->lokasi->sektor_id == $sektor['id'])
                                @php $i++; @endphp
                                <tr>
                                    <td align="center">{{ $i }}</td>
                                    <td>{{ $t->lokasi->lokasi }}</td>
                                    <td>{{ $t->barang->kategori->kategori }}</td>
                                    <td>{{ $t->barang->merk }}</td>
                                    <td>{{ $t->barang->tipe }}</td>
                                    <td><center>{{ $t->jml_barang }}</center></td>
                                    <td>{{ $t->barang->produsen->produsen }}</td>
                                    <td>{{ ucwords($t->barang->keterangan)." "."Terverifikasi" }}</td>
                                    <form method="post" action="{{ route('transaksi.delete', ['transaksi' => $t->id]) }}">
                                        @csrf @method('delete')
                                        <td>
                                            <center>
                                            <a href="{{ route('transaksi.edit', ['transaksi' => $t->id]) }}" class="btn blue btn-xs"><i class="fa fa-pencil"></i></a>
                                            <button type="submit" class="btn red btn-xs" onclick="return confirm('Anda yakin ingin menghapus entri ini?');"><i class="fa fa-trash"></i></button>
                                            </center>
                                        </td>
                                    </form>
                                </tr>
                            @endif
                        @endforeach
                    </table>
                </div>
            </div>
        </div>
        <!-- END CONTENT -->
    </div>
@endsection