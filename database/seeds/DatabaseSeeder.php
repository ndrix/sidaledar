<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        $this->call(SektorSeeder::class);
        $this->call(LokasiSeeder::class);
        $this->call(JenisBarangSeeder::class);
        $this->call(KategoriSeeder::class);
        $this->call(ProdusenSeeder::class);
        $this->call(BarangSeeder::class);
        $this->call(TransaksiSeeder::class);
    }
}
