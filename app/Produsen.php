<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Produsen extends Model
{
    protected $table = 'produsens';

    protected $fillable = [
        'produsen', 'nomor_izin'
    ];

    public function barang()
    {
        return $this->hasMany('App\Barang', 'produsen_id');
    }
}
