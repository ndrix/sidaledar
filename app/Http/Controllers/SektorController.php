<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Sektor;
use App\Transaksi;
use App\Http\Requests\SektorRequest;
use Auth;

class SektorController extends Controller
{
    public function index(Request $request)
    {
        $keyword                    = $request->get('search');
        $transaksi                  = Transaksi::all();

        //Fungsi search di halaman index
        if($keyword != null){
        $transaksi                  = Transaksi::whereHas('lokasi', function($query) use($keyword) { $query->where('lokasi', 'like', '%'.$keyword.'%'); })
                                        ->orWhereHas('barang', 
                                            function($query) use($keyword) { 
                                                $query->whereHas('kategori',  
                                                    function($query) use($keyword) { 
                                                        $query->where('kategori', 'like', '%'.$keyword.'%');
                                                    }
                                                );
                                            })
                                        ->orWhereHas('barang', function($query) use($keyword) { $query->where('merk', 'like', '%'.$keyword.'%'); })
                                        ->orWhereHas('barang', function($query) use($keyword) { $query->where('tipe', 'like', '%'.$keyword.'%'); })
                                        ->orWhereHas('barang', 
                                            function($query) use($keyword) { 
                                                $query->whereHas('produsen',  
                                                    function($query) use($keyword) { 
                                                        $query->where('produsen', 'like', '%'.$keyword.'%');
                                                    }
                                                );
                                            })
                                        ->orWhereHas('barang', function($query) use($keyword) { $query->where('keterangan', 'like', '%'.$keyword.'%'); })
                                        ->get();
        }

        return view('sektor.index', ['transaksi' => $transaksi, 'keyword' => $keyword]);
    }

    public function daftar(Request $request)
    {
        $keyword                    = $request->get('search');
        $sektor                     = Sektor::all();

        //Fungsi search di halaman index
        if($keyword != null){
            $sektor                 = Sektor::where('sektor', 'like', '%'.$keyword.'%')->get();
        }

        return view('sektor.list', ['sektor' => $sektor, 'keyword' => '']);
    }

    public function show($id, Request $request)
    {
        $keyword                    = $request->get('search');
        $sektor                     = Sektor::where('id', $id)->first();
        $transaksi                  = Transaksi::all();

        //Fungsi search di halaman show
        if($keyword != null){
            $transaksi              = Transaksi::whereHas('lokasi', function($query) use($keyword) { $query->where('lokasi', 'like', '%'.$keyword.'%'); })
                                        ->orWhereHas('barang', 
                                            function($query) use($keyword) { 
                                                $query->whereHas('kategori',  
                                                    function($query) use($keyword) { 
                                                        $query->where('kategori', 'like', '%'.$keyword.'%');
                                                    }
                                                );
                                            })
                                        ->orWhereHas('barang', function($query) use($keyword) { $query->where('merk', 'like', '%'.$keyword.'%'); })
                                        ->orWhereHas('barang', function($query) use($keyword) { $query->where('tipe', 'like', '%'.$keyword.'%'); })
                                        ->orWhereHas('barang', 
                                            function($query) use($keyword) { 
                                                $query->whereHas('produsen',  
                                                    function($query) use($keyword) { 
                                                        $query->where('produsen', 'like', '%'.$keyword.'%');
                                                    }
                                                );
                                            })
                                        ->orWhereHas('barang', function($query) use($keyword) { $query->where('keterangan', 'like', '%'.$keyword.'%'); })
                                        ->get();
        }

        return view('sektor.show', ['transaksi' => $transaksi, 'keyword' => $keyword, 'sektor' => $sektor]);
    }

    public function create()
    {
        return view('sektor.create');
    }

    public function store(SektorRequest $request)
    {
        $sektor['sektor']           = $request->get('nama');
        Sektor::create($sektor);

        $log['jenis'] = "add";
        $log['log']   = Auth::user()->name." melakukan penambahan sektor.";
        \App\Log::create($log);
        
        return redirect()->back()->with('success', 'Berhasil menambahkan sektor baru.');
    }

    public function edit(Sektor $sektor)
    {
        return view('sektor.edit', ['sektor' => $sektor]);
    }

    public function update(Sektor $sektor, SektorRequest $request)
    {
        $sektor['sektor']           = $request->get('nama');
        $sektor->save();

        $log['jenis'] = "update";
        $log['log']   = Auth::user()->name." melakukan pengubahan data sektor.";
        \App\Log::create($log);
        
        return redirect()->back()->with('success', 'Berhasil mengubah data sektor.');
    }

    public function destroy(Sektor $sektor)
    {
        $sektor->delete();

        $log['jenis'] = "delete";
        $log['log']   = Auth::user()->name." melakukan penghapusan data sektor.";
        \App\Log::create($log);

        return redirect()->back()->with('success', 'Berhasil menghapus data sektor.');
    }
}
