@extends('template')

@section('content')
    <div class="page-content">
        <!-- BEGIN BREADCRUMBS -->
        <div class="breadcrumbs">
            <h1>{{ ucwords($jenis['jenis']) }}</h1>
            <ol class="breadcrumb">
                <li>
                    <a href="{{ route('home') }}">Home</a>
                </li>
                <li>
                    <a href="">Data</a>
                </li>
                <li class="active">
                    Perangkat
                </li>
            </ol>
        </div>
        <!-- END BREADCRUMBS -->
        <!-- BEGIN CONTENT -->
        <div class="row">
            <form action="{{ url()->current() }}">
                <div class="col-md-4">
                    @if($keyword != null)
                        <input type="text" class="form-control" name="search" placeholder="Cari..." value="{{ $keyword }}">
                    @else
                        <input type="text" class="form-control" name="search" placeholder="Cari...">
                    @endif
                </div>
                <div class="col-md-2">
                    <button type="submit" class="btn blue">Cari</button>
                    @php $route_name = \Request::route()->getName(); @endphp
                    <a href="{{ route($route_name, ['id' => $jenis['id']]) }}" class="btn default">Reset</a>
                </div>
            </form>
        </div><br>
        @if ($keyword != NULL)
            <div class="row">
                <div class="col-md-12">
                    <div class="alert alert-info">
                        <p>Menampilkan hasil pencarian untuk <b>"{{ $keyword }}"</b></p>
                    </div>
                </div>
            </div>
        @endif
        <div class="row">
            <div class="col-md-12">
                <div class="table-scrollable">
                    <table class="table table-striped table-bordered table-hover">
                        <tr>
                            <th width="5%"><center>No</center></th>
                            <th width="20%"><center>Merk</center></th>
                            <th width="20%"><center>Tipe</center></th>
                            <th width="15%"><center>Kategori</center></th>
                            <th width="20%"><center>Produsen</center></th>
                            <th width="13%"><center>Keterangan</center></th>
                            <th width="7%"></th>
                        </tr>
                        @php $i = 0; @endphp
                        @foreach($barang as $b)
                            @if($b->kategori->jenis_id == $jenis['id'])
                                @php $i++; @endphp
                                <tr>
                                    <td align="center">{{ $loop->iteration }}</td>
                                    <td>{{ $b->merk }}</td>
                                    <td>{{ $b->tipe }}</td>
                                    <td>{{ $b->kategori->kategori }}</td>
                                    <td>{{ $b->produsen->produsen }}</td>
                                    <td>{{ ucwords($b->keterangan)." "."Tersertifikasi" }}</td>
                                    <form method="post" action="{{ route('barang.delete', ['barang' => $b->id]) }}">
                                        @csrf @method('delete')
                                        <td>
                                            <center>
                                            <a href="{{ route('barang.edit', ['barang' => $b->id]) }}" class="btn blue btn-xs"><i class="fa fa-pencil"></i></a>
                                            <button type="submit" class="btn red btn-xs" onclick="return confirm('Anda yakin ingin menghapus entri ini?');"><i class="fa fa-trash"></i></button>
                                            </center>
                                        </td>
                                    </form>
                                </tr>
                            @endif
                        @endforeach
                    </table>
                </div>
            </div>
        </div>
        <!-- END CONTENT -->
    </div>
@endsection