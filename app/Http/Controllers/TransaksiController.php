<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Transaksi;
use App\Lokasi;
use App\Barang;
use App\Http\Requests\TransaksiRequest;
use Auth;

class TransaksiController extends Controller
{
    public function index(Request $request)
    {
        $keyword                        = $request->get('keyword');

        //Fungsi search di halaman index
        if($keyword != null){
            $transaksi                  = Transaksi::whereHas('lokasi', function($query) use($keyword) { $query->where('lokasi', 'like', '%'.$keyword.'%'); })
                                            ->orWhereHas('barang', 
                                                function($query) use($keyword) { 
                                                    $query->whereHas('kategori',  
                                                        function($query) use($keyword) { 
                                                            $query->where('kategori', 'like', '%'.$keyword.'%');
                                                        }
                                                    );
                                                })
                                            ->orWhereHas('barang', function($query) use($keyword) { $query->where('merk', 'like', '%'.$keyword.'%'); })
                                            ->orWhereHas('barang', function($query) use($keyword) { $query->where('tipe', 'like', '%'.$keyword.'%'); })
                                            ->orWhereHas('barang', 
                                                function($query) use($keyword) { 
                                                    $query->whereHas('produsen',  
                                                        function($query) use($keyword) { 
                                                            $query->where('produsen', 'like', '%'.$keyword.'%');
                                                        }
                                                    );
                                                })
                                            ->orWhereHas('barang', function($query) use($keyword) { $query->where('keterangan', 'like', '%'.$keyword.'%'); })
                                            ->get();
        } else {
            $transaksi                  = Transaksi::all();
        }
        
        return view('transaksi.index', ['transaksi' => $transaksi, 'keyword' => $keyword]);
    }

    public function create()
    {
        $lokasi                         = Lokasi::all();
        $barang                         = Barang::all();
        
        return view('transaksi.create', ['barang' => $barang, 'lokasi' => $lokasi]);
    }

    public function store(TransaksiRequest $request)
    {
        $transaksi['lokasi_id']         = $request->get('lokasi_id');
        $transaksi['barang_id']         = $request->get('barang_id');
        $transaksi['jml_barang']        = $request->get('jumlah');
        Transaksi::create($transaksi);

        $log['jenis'] = "add";
        $log['log']   = Auth::user()->name." melakukan penambahan transaksi.";
        \App\Log::create($log);

        return redirect()->back()->with('success', 'Berhasil menambahkan transaksi baru.');
    }

    public function edit(Transaksi $transaksi)
    {
        $lokasi                         = Lokasi::all();
        $barang                         = Barang::all();

        return view('transaksi.edit', ['transaksi' => $transaksi, 'lokasi' => $lokasi, 'barang' => $barang]);
    }

    public function update(Transaksi $transaksi, TransaksiRequest $request)
    {
        $transaksi->lokasi_id           = $request->get('lokasi_id');
        $transaksi->barang_id           = $request->get('barang_id');
        $transaksi->jml_barang          = $request->get('jumlah');
        $transaksi->save();

        $log['jenis'] = "update";
        $log['log']   = Auth::user()->name." melakukan pengubahan data lokasi.";
        \App\Log::create($log);

        return redirect()->back()->with('success', 'Berhasil mengubah data transaksi.');
    }

    public function destroy(Transaksi $transaksi)
    {
        $transaksi->delete();

        $log['jenis'] = "delete";
        $log['log']   = Auth::user()->name." melakukan penghapusan data lokasi.";
        \App\Log::create($log);

        return redirect()->back()->with('success', 'Berhasil menghapus data transaksi.');
    }
}
