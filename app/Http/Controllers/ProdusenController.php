<?php

namespace App\Http\Controllers;

use Auth;
use Illuminate\Http\Request;
use App\Produsen;
use App\Barang;
use App\JenisBarang;
use App\Http\Requests\ProdusenRequest;

class ProdusenController extends Controller
{
    public function index(Request $request)
    {
        $keyword                    = $request->get('search');
        $produsen                   = Produsen::all();

        //Fungsi search di halaman index
        if($keyword != null){
            $produsen               = Produsen::where('produsen', 'like', '%'.$keyword.'%')
                                        ->get();
        }
        
        return view('produsen.index', ['produsen' => $produsen, 'keyword' => $keyword]);
    }

    public function show($id, Request $request)
    {
        $keyword                    = $request->get('search');
        $barang                     = Barang::all();
        $jenis                      = JenisBarang::where('id', $id)->first();
        $produsen                   = array();

        foreach($barang as $b){
            if($b->kategori->jenis_id == $id) {
                //Fungsi search di halaman show
                if($keyword != null){
                    $produsen[]     = Produsen::where('produsen', 'like', '%'.$keyword.'%')
                                        ->where('id', $b->produsen_id)
                                        ->first();
                    if($produsen == [null]){
                        $produsen = [];
                    }
                } else {
                    $produsen[]     = Produsen::where('id', $b->produsen_id)->first();
                }
            } 
            else {
                $produsen           = [];
            }
        }
        // jika tidak ada barang
        if(Barang::first() == []){
            $produsen = [];
        }
        // menghilangkan duplikat di array produsen
        $produsen = array_unique($produsen);

        return view('produsen.show', ['produsen' => $produsen, 'jenis' => $jenis, 'keyword' => $keyword]);
    }

    public function create()
    {
        return view('produsen.create');
    }

    public function store(ProdusenRequest $request)
    {
        $produsen['produsen']       = $request->get('nama');
        $produsen['nomor_izin']     = $request->get('nomor_izin');
        Produsen::create($produsen);

        $log['jenis'] = "add";
        $log['log']   = Auth::user()->name." melakukan penambahan produsen.";
        \App\Log::create($log);

        return redirect()->back()->with('success', 'Berhasil menambahkan produsen baru.');
    }

    public function edit(Produsen $produsen)
    {
        return view('produsen.edit', ['produsen' => $produsen]);
    }

    public function update(Produsen $produsen, ProdusenRequest $request)
    {
        $produsen->produsen         = $request->get('nama');
        $produsen->nomor_izin       = $request->get('nomor_izin');
        $produsen->save();

        $log['jenis'] = "update";
        $log['log']   = Auth::user()->name." melakukan penambahan data produsen.";
        \App\Log::create($log);

        return redirect()->back()->with('success', 'Berhasil mengubah data produsen.');
    }

    public function destroy(Produsen $produsen)
    {
        $produsen->delete();

        $log['jenis'] = "delete";
        $log['log']   = Auth::user()->name." melakukan penghapusan data produsen.";
        \App\Log::create($log);

        return redirect()->back()->with('success', 'Berhasil menghapus data produsen.');
    }
}
