<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLokasisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lokasis', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('sektor_id')->unsigned();
            $table->string('lokasi');
            $table->string('akta_pendirian')->nullable();
            $table->string('rincian')->nullable();
            $table->string('nama_personil')->nullable();
            $table->string('cara_perolehan')->nullable();
            $table->timestamps();

            $table->foreign('sektor_id')->references('id')->on('sektors')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sektor_kategoris');
    }
}
