<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Barang extends Model
{
    protected $table = 'barangs';

    protected $fillable = [
        'kategori_id', 'produsen_id', 'tipe', 'merk', 'keterangan'
    ];

    public function kategori()
    {
        return $this->belongsTo('App\Kategori', 'kategori_id');
    }

    public function produsen()
    {
        return $this->belongsTo('App\Produsen', 'produsen_id');
    }

    public function transaksi()
    {
        return $his->hasMany('App\Transaksi', 'barang_id');
    }
}
