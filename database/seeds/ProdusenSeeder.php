<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class ProdusenSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('produsens')->insert(array(
            array(
                'produsen'      => 'PT. Indoguardika Cipta Kreasi',
                'created_at'    => Carbon::now(),
                'updated_at'    => Carbon::now(),
            ),
            array(
                'produsen'      => 'PT. Guna Multi Mandiri',
                'created_at'    => Carbon::now(),
                'updated_at'    => Carbon::now(),
            ),
        ));
    }
}
