<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('dashboard');
// });

Route::get('/', 'DashboardController@home')->name('home')->middleware('ceklogin');

Route::get('analisis', 'DashboardController@index')->name('analisis')->middleware('ceklogin');

Route::get('sektor/all', 'SektorController@index')->name('sektor.index')->middleware('ceklogin');

Route::get('sektor/create', 'SektorController@create')->name('sektor.create')->middleware('ceklogin');

Route::post('sektor/store', 'SektorController@store')->name('sektor.store')->middleware('ceklogin');

Route::get('sektor/list', 'SektorController@daftar')->name('sektor.list')->middleware('ceklogin');

Route::get('sektor/edit/{sektor}', 'SektorController@edit')->name('sektor.edit')->middleware('ceklogin');

Route::put('sektor/update/{sektor}', 'SektorController@update')->name('sektor.update')->middleware('ceklogin');

Route::delete('sektor/delete/{sektor}', 'SektorController@destroy')->name('sektor.delete')->middleware('ceklogin');

Route::get('sektor/{id}', 'SektorController@show')->name('sektor.show')->middleware('ceklogin');

Route::get('lokasi/all', 'LokasiController@index')->name('lokasi.index')->middleware('ceklogin');

Route::get('lokasi/create', 'LokasiController@create')->name('lokasi.create')->middleware('ceklogin');

Route::post('lokasi/store', 'LokasiController@store')->name('lokasi.store')->middleware('ceklogin');

Route::get('lokasi/edit/{lokasi}', 'LokasiController@edit')->name('lokasi.edit')->middleware('ceklogin');

Route::put('lokasi/update/{lokasi}', 'LokasiController@update')->name('lokasi.update')->middleware('ceklogin');

Route::delete('lokasi/edit/{lokasi}', 'LokasiController@destroy')->name('lokasi.delete')->middleware('ceklogin');

Route::get('kategori/all', 'KategoriController@index')->name('kategori.index')->middleware('ceklogin');

Route::get('kategori/create', 'KategoriController@create')->name('kategori.create')->middleware('ceklogin');

Route::post('kategori/store', 'KategoriController@store')->name('kategori.store')->middleware('ceklogin');

Route::get('kategori/edit/{kategori}', 'KategoriController@edit')->name('kategori.edit')->middleware('ceklogin');

Route::put('kategori/update/{kategori}', 'KategoriController@update')->name('kategori.update')->middleware('ceklogin');

Route::delete('kategori/delete/{kategori}', 'KategoriController@destroy')->name('kategori.delete')->middleware('ceklogin');

// Route::get('jenis/all', 'JenisController@index')->name('jenis.index')->middleware('ceklogin');

// Route::get('jenis/create', 'JenisController@create')->name('jenis.create')->middleware('ceklogin');

// Route::post('jenis/store', 'JenisController@store')->name('jenis.store')->middleware('ceklogin');

Route::get('jenis/{jenis}', 'JenisController@show')->name('jenis.show')->middleware('ceklogin');

// Route::get('jenis/edit/{jenis}', 'JenisController@edit')->name('jenis.edit')->middleware('ceklogin');

// Route::put('jenis/update/{jenis}', 'JenisController@update')->name('jenis.update')->middleware('ceklogin');

// Route::delete('jenis/delete/{jenis}', 'JenisController@destroy')->name('jenis.delete')->middleware('ceklogin');

Route::get('produsen/all', 'ProdusenController@index')->name('produsen.index')->middleware('ceklogin');

Route::get('produsen/create', 'ProdusenController@create')->name('produsen.create')->middleware('ceklogin');

Route::post('produsen/store', 'ProdusenController@store')->name('produsen.store')->middleware('ceklogin');

Route::get('produsen/edit/{produsen}', 'ProdusenController@edit')->name('produsen.edit')->middleware('ceklogin');

Route::put('produsen/update/{produsen}', 'ProdusenController@update')->name('produsen.update')->middleware('ceklogin');

Route::delete('produsen/delete/{produsen}', 'ProdusenController@destroy')->name('produsen.delete')->middleware('ceklogin');

Route::get('produsen/{id}', 'ProdusenController@show')->name('produsen.show')->middleware('ceklogin');

Route::get('barang/all', 'BarangController@index')->name('barang.index')->middleware('ceklogin');

Route::get('barang/create', 'BarangController@create')->name('barang.create')->middleware('ceklogin');

Route::post('barang/store', 'BarangController@store')->name('barang.store')->middleware('ceklogin');

Route::get('barang/edit/{barang}', 'BarangController@edit')->name('barang.edit')->middleware('ceklogin');

Route::put('barang/update/{barang}', 'BarangController@update')->name('barang.update')->middleware('ceklogin');

Route::delete('barang/delete/{barang}', 'BarangController@destroy')->name('barang.delete')->middleware('ceklogin');

Route::get('transaksi/all', 'TransaksiController@index')->name('transaksi.index')->middleware('ceklogin');

Route::get('transaksi/create', 'TransaksiController@create')->name('transaksi.create')->middleware('ceklogin');

Route::post('transaksi/store', 'TransaksiController@store')->name('transaksi.store')->middleware('ceklogin');

Route::get('transaksi/edit/{transaksi}', 'TransaksiController@edit')->name('transaksi.edit')->middleware('ceklogin');

Route::put('transaksi/update/{transaksi}', 'TransaksiController@update')->name('transaksi.update')->middleware('ceklogin');

Route::delete('transaksi/delete/{transaksi}', 'TransaksiController@destroy')->name('transaksi.delete')->middleware('ceklogin');

Route::get('user/all', 'UserController@index')->name('user.index')->middleware('ceklogin');

Route::get('user/create', 'UserController@create')->name('user.create')->middleware('ceklogin');

Route::post('user/store', 'UserController@store')->name('user.store')->middleware('ceklogin');

Route::get('user/edit/{user}', 'UserController@edit')->name('user.edit')->middleware('ceklogin');

Route::put('user/update/{user}', 'UserController@update')->name('user.update')->middleware('ceklogin');

Route::delete('user/delete/{user}', 'UserController@delete')->name('user.delete')->middleware('ceklogin');

Route::get('logs', 'LogController@index')->name('log.index')->middleware('ceklogin');

Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');
