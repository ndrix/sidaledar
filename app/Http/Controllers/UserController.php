<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Http\Requests\UserRequest;
use App\Http\Requests\UserUpdateRequest;
use Auth;

class UserController extends Controller
{
    public function index(Request $request)
    {
        $keyword                    = $request->get('search');

        //Fungsi search di halaman index
        if($keyword != null){
            $user                   = User::where('name', 'like', '%'.$keyword.'%')
                                        ->orWhere('username', 'like', '%'.$keyword.'%')
                                        ->orWhere('email', 'like', '%'.$keyword.'%')
                                        ->get();
        } else {
            $user                   = User::all();
        }

        return view('user.index', ['user' => $user, 'keyword' => $keyword]);
    }

    public function create()
    {
        return view('user.create');
    }

    public function store(UserRequest $request)
    {
        $user['name']               = $request->get('name');
        $user['username']           = $request->get('username');
        $user['email']              = $request->get('email');
        $user['password']           = bcrypt($request->get('password'));
        User::create($user);

        $log['jenis'] = "add";
        $log['log']   = Auth::user()->name." melakukan penambahan user.";
        \App\Log::create($log);

        return redirect()->back()->with('success', 'Berhasil menambahkan user baru.');
    }

    public function edit(User $user)
    {
        return view('user.edit', ['user' => $user]);
    }

    public function update(UserUpdateRequest $request, User $user)
    {
        $user->name                 = $request->get('name');
        $user->username             = $request->get('username');
        $user->email                = $request->get('email');
        if($request->get('password') != null){
            $user->password             = bcrypt($request->get('password'));
        }
        $user->save();

        $log['jenis'] = "update";
        $log['log']   = Auth::user()->name." melakukan pengubahan data user.";
        \App\Log::create($log);

        return redirect()->back()->with('success', 'Berhasil mengubah data user.');
    }

    public function delete(User $user)
    {
        if($user->id != 1){
            $user->delete();

            $log['jenis'] = "delete";
            $log['log']   = Auth::user()->name." melakukan penghapusan data user.";
            \App\Log::create($log);
        } else {
            return redirect()->back()->with('error', 'Maaf, akun administrator tidak dapat dihapus.');
        }

        return redirect()->back()->with('success', 'Berhasil menghapus data user.');
    }
}
