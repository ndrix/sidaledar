<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Barang;
use App\JenisBarang;
use App\Http\Requests\JenisBarangRequest;
use Auth;

class JenisController extends Controller
{
    public function index(Request $request)
    {
        $keyword                    = $request->get('search');

        //Fungsi search di halaman index
        if($keyword != null){
            $jenis                  = JenisBarang::all();
        } else {
            $jenis                  = JenisBarang::where('jenis', 'like', '%'.$keyword.'%')->get();
        }

        return view('jenis.index', ['jenis' => $jenis, 'keyword' => $keyword]);
    }

    public function show($id, Request $request)
    {
        $keyword                    = $request->get('search');
        $barang                     = Barang::all();
        $jenis                      = JenisBarang::where('id', $id)->first();

        //Fungsi search di halaman show
        if($keyword != null){
            $barang                 = Barang::whereHas('kategori', function($query) use($keyword) { $query->where('kategori', 'like', '%'.$keyword.'%'); })
                                        ->orWhere('merk', 'like', '%'.$keyword.'%')
                                        ->orWhere('tipe', 'like', '%'.$keyword.'%')
                                        ->orWhereHas('produsen', function($query) use($keyword) { $query->where('produsen', 'like', '%'.$keyword.'%'); })
                                         ->orWhere('keterangan', 'like', '%'.$keyword.'%')
                                        ->get();
        }

        return view('jenis.show', ['barang' => $barang, 'jenis' => $jenis, 'keyword' => $keyword]);
    }

    public function create()
    {
        return view('jenis.create');
    }

    public function store(JenisBarangRequest $request)
    {
        $jenis['jenis']             = $request->get('nama');
        JenisBarang::create($jenis);

        $log['jenis'] = "add";
        $log['log']   = Auth::user()->name." melakukan penambahan jenis barang.";
        \App\Log::create($log);

        return redirect()->back()->with('success', 'Berhasil menambahkan jenis baru.');
    }

    public function edit(JenisBarang $jenis)
    {
        return view('jenis.edit', ['jenis' => $jenis]);
    }

    public function update(JenisBarang $jenis, JenisBarangRequest $request)
    {
        $jenis->jenis                = $request->get('nama');
        $jenis->save();

        $log['jenis'] = "update";
        $log['log']   = Auth::user()->name." melakukan pengubahan data jenis barang.";
        \App\Log::create($log);

        return redirect()->back()->with('success', 'Berhasil mengubah data jenis perangkat.');
    }

    public function destroy(JenisBarang $jenis)
    {
        $jenis->delete();

        $log['jenis'] = "delete";
        $log['log']   = Auth::user()->name." melakukan penghapusan data jenis barang.";
        \App\Log::create($log);

        return redirect()->back()->with('success', 'Berhasil menghapus data jenis perangkat.');
    }
}
