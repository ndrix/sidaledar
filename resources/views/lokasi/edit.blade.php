@extends('template')

@section('content')
    <div class="page-content">
        <!-- BEGIN BREADCRUMBS -->
        <div class="breadcrumbs">
            <h1>Ubah Lokasi</h1>
            <ol class="breadcrumb">
                <li>
                    <a href="{{ route('home') }}">Home</a>
                </li>
                <li>
                    <a href="">Data</a>
                </li>
                <li class="active">
                    Sektor
                </li>
            </ol>
        </div>
        <!-- END BREADCRUMBS -->
        <!-- BEGIN CONTENT -->
        @if ($message = Session::get('success'))
            <div class="alert alert-success alert-dismissable">
                <p>{{ $message }}</p>
            </div>
        @endif
        @if ($message = Session::get('error'))
            <div class="alert alert-danger alert-dismissable">
                <p>{{ $message }}</p>
            </div>
        @endif
        @if ($errors->any())
            <div class="alert alert-danger">
                <strong>Whoops!</strong> Something wrong with your inputs.<br><br>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <div class="row">
            <div class="col-md-12">
            <form action="{{ route('lokasi.update', ['lokasi' => $lokasi->id]) }}" method="post">
                @csrf @method('put')
                <div class="form-body">
                    <div class="form-group">
                        <label class="control-label">Nama Lokasi</label>
                        <input type="text" class="form-control" name="nama" value="{{ $lokasi->lokasi }}">
                    </div>
                    <div class="form-group">
                        <label class="control-label">Sektor</label>
                        <select class="bs-select form-control" name="sektor_id">
                            <option value=""></option>
                            @foreach($sektor as $s)
                                @if($lokasi->sektor_id == $s->id)
                                    <option value="{{ $s->id }}" selected>{{ $s->sektor }}</option>
                                @else
                                    <option value="{{ $s->id }}">{{ $s->sektor }}</option>
                                @endif
                            @endforeach
                        </select>
                    </div>
                </div><hr>
                <div class="form-actions">
                    <center>
                        <button type="submit" class="btn green">Simpan</button>
                        <a href="{{ URL::previous()  }}" class="btn default">Batal</a>
                    </center>
                </div>
            </form>
            </div>
        </div>
    </div>
@endsection